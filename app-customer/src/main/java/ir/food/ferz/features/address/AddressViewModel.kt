package ir.food.ferz.features.home

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.androidnetworking.error.ANError
import ir.food.ferz.data.model.request.AddAddressBody
import ir.food.ferz.data.model.response.GetAddressResponse
import ir.food.ferz.data.model.response.GetCityResponse
import ir.food.ferz.data.model.response.GetProvinceResponse
import ir.food.ferz.data.model.response.VerifyResponse
import ir.food.ferz.features.base.BaseViewModel
import java.util.*

class AddressViewModel : BaseViewModel(), LifecycleObserver {


    var getProvinceResult = MutableLiveData<List<GetProvinceResponse>>()
    var getCityResult = MutableLiveData<List<GetCityResponse>>()
    var addAddressResult = MutableLiveData<VerifyResponse>()
    var getAddressResult = MutableLiveData<List<GetAddressResponse>>()
    // ************* Live Data

    override fun init() {

    }

    fun getProvince() {
        appRepository.getProvince(getProvinceResult, isLoadingApi, errorApi)
    }

    fun getCity(provinceId: String) {
        appRepository.getCity(provinceId, getCityResult, isLoadingApi, errorApi)
    }

    fun getAddAddress(addAddressBody: AddAddressBody) {
        appRepository.getAddAddress(addAddressBody, addAddressResult, isLoadingApi, errorApi)
    }

    fun getAddress() {
        appRepository.getAddress(
            appRepository.preferencesHelper.userId.toString(),
            getAddressResult,
            isLoadingApi,
            errorApi
        )
    }
}
