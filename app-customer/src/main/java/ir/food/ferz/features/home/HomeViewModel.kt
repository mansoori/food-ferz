package ir.food.ferz.features.home

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.androidnetworking.error.ANError
import ir.food.ferz.data.model.response.GetFoodTypeResponse
import ir.food.ferz.features.base.BaseViewModel

class HomeViewModel  : BaseViewModel(), LifecycleObserver {

    // ************* Live Data
    var getFoodTypeResult = MutableLiveData<List<GetFoodTypeResponse>>()


    // ************* Live Data

   override fun init() {

    }
fun getFoodType(foodCategoriId:String){
    appRepository.getFoodType(foodCategoriId,getFoodTypeResult,isLoadingApi,errorApi)
}


}
