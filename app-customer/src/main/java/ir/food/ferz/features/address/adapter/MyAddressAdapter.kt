package ir.food.ferz.features.address.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mapbox.mapboxsdk.geometry.LatLng
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.data.model.response.GetAddressResponse
import ir.food.ferz.shared.utils.DialogUtils
import ir.food.ferz.shared.utils.MapUtils
import timber.log.Timber


class MyAddressAdapter(val mContext: Context, private val itemList: List<GetAddressResponse>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as ViewHolderRow).bindItem(itemList, position)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater
            .from(mContext)
            .inflate(R.layout.row_address, parent, false)

        return ViewHolderRow(view)


    }

    override fun getItemCount(): Int {

        return itemList.size
    }


    class ViewHolderRow(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(itemList: List<GetAddressResponse>, position: Int) {

            val item = itemList[position]

            val mapScreen = itemView.findViewById(R.id.screenMap) as ImageView
            val shortaddress = itemView.findViewById(R.id.shortaddress) as TextView
            val fullAddress = itemView.findViewById(R.id.fullAddress) as TextView
            val selectedRate = itemView.findViewById(R.id.selectRatingBar) as RatingBar

            var finalHeight: Int
            var finalWidth: Int

            val vto = mapScreen.viewTreeObserver
            vto.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    mapScreen.viewTreeObserver.removeOnPreDrawListener(this)
                    finalHeight = mapScreen.measuredHeight
                    finalWidth = mapScreen.measuredWidth
                    Timber.d("SizeImage====>" + "Height: $finalHeight Width: $finalWidth")

                    item.lat?.let {

                        Glide.with(itemView.context)
                            .load(
                                MapUtils.getMapScreenShot(
                                    item.lat.toDouble(),
                                    item.lng.toDouble(),
                                    finalWidth,
                                    finalHeight
                                )
                            )
                            .asBitmap()
                            .into(mapScreen)
                        Timber.d(
                            "SizeImage==ttttttt==>" + MapUtils.getMapScreenShot(
                                item.lat.toDouble(),
                                item.lng.toDouble(),
                                finalWidth - 5,
                                finalHeight
                            )
                        )


                    }
                    return true
                }
            })






            shortaddress.text = item.city
            fullAddress.text = item.address
            selectedRate.isSelected = item.isSelect

//todo fix delet address for action

            itemView.setOnClickListener {
                item.lat?.let {
                    var latLng = LatLng(item.lat.trim().toDouble(), item.lng.trim().toDouble())
                    App.getBus().post(
                        OttoToken(
                            BusActionEnum.UPDATE_ADDRESS, latLng
                        )
                    )

                }

            }
            itemView.setOnLongClickListener(object :View.OnLongClickListener{
                override fun onLongClick(v: View?): Boolean {

                    DialogUtils.getInstance().showDialodAddresOption()
                   return true

                }
            })

        }

    }
}

