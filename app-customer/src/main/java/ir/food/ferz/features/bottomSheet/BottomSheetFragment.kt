package ir.food.ferz.features.main.bottomSheet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.food.ferz.R
import ir.food.ferz.features.bottomSheet.enums.BottomSheetAdapterType
import ir.namaa.app.Add_Saved.view.adapter.BottomSheetAdapter
import ir.food.ferz.features.main.bottomSheet.data.BottomSheetDataModel
import kotlinx.android.synthetic.main.bottom_sheet_fragment.view.*


class BottomSheetFragment : BottomSheetDialogFragment() {

    fun layout() = R.layout.bottom_sheet_fragment
    fun tag(): String = this::class.java.simpleName

    private lateinit var bottomSheetAdapter: BottomSheetAdapter

    companion object {
        lateinit var type: BottomSheetAdapterType
        lateinit var data: Any
        private var instance: BottomSheetFragment? = null

        fun newInstance(type: BottomSheetAdapterType, data: Any?): BottomSheetFragment {
            this.type = type
            this.data = data!!
            if (instance == null)
                instance = BottomSheetFragment()

            return instance!!
        }
    }


    override fun onCreateView(i: LayoutInflater, c: ViewGroup?, si: Bundle?): View? {
        val rootView = i.inflate(layout(), c, false)
        dialog.window
            .attributes.windowAnimations = R.style.PauseDialogAnimation
        bottomSheetAdapter = BottomSheetAdapter(activity!!)

        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);// here i have fragment height 30% of window's height you can set it as per your requirement

        val list = ArrayList<BottomSheetDataModel>()
        list.add(BottomSheetDataModel(type, data))
        bottomSheetAdapter.setcallPOIFeed(list)
        rootView.recycler_view.adapter = bottomSheetAdapter
        bottomSheetAdapter.notifyDataSetChanged()

// This listener's onShow is fired when the dialog is shown
        dialog.setOnShowListener { dialog ->
            // In a previous life I used this method to get handles to the positive and negative buttons
            // of a dialog in order to change their Typeface. Good ol' days.

            val d = dialog as BottomSheetDialog

            // This is gotten directly from the source of BottomSheetDialog
            // in the wrapInBottomSheet() method
            val bottomSheet = d.findViewById<View>(ir.food.ferz.R.id.design_bottom_sheet) as FrameLayout?

            // Right here!
            BottomSheetBehavior.from(bottomSheet!!).state = BottomSheetBehavior.STATE_EXPANDED
        }

        return rootView
    }


}