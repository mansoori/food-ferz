package ir.food.ferz.features.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.mapbox.mapboxsdk.geometry.LatLng
import com.squareup.otto.Subscribe
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.databinding.FragmentAddressBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.address.adapter.MyAddressAdapter
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.address.map.MapFragment
import kotlinx.android.synthetic.main.fragment_address.*
import timber.log.Timber
import javax.inject.Inject


class MyAddressFragment : BaseFragment<FragmentAddressBinding, AddressViewModel>() {
    fun onClick(v: View?) {

    }

    override fun onResume() {
        super.onResume()
        App.getBus().register(this)
    }

    override fun onPause() {
        super.onPause()
        App.getBus().unregister(this)
    }

    override fun getViewModel(): AddressViewModel = addressViewModel

    override fun initView(view: View) {
        getViewModel().getAddress()
    }

    override fun initVmObservers() {
        getViewModel().getAddressResult.observe(this, Observer {
            var addressAdapter = MyAddressAdapter(activity!!, it)
            recyclerView.adapter = addressAdapter
        })
    }

    @Inject
    lateinit var addressViewModel: AddressViewModel

    override fun layout() = R.layout.fragment_address

    companion object {
        fun newInstance() = MyAddressFragment()
    }

    @Subscribe
    fun answerAvailable(event: OttoToken) {
        Timber.d("answerAvailable : " + event.action?.name.toString())
        when (event.action) {
            BusActionEnum.UPDATE_ADDRESS -> {

                val bundle = Bundle()
                var latlng=event.obj as LatLng
                bundle.putString("lat",latlng.latitude.toString() )
                bundle.putString("lng",latlng.longitude.toString() )
                MapFragment.newInstance().arguments = bundle

                (requireActivity() as MainActivity).showFragment(MapFragment.newInstance())

            }


        }
    }

}
