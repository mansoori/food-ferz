package ir.namaa.app.Add_Saved.view.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.R
import ir.food.ferz.features.main.bottomSheet.data.BottomSheetDataModel
import ir.food.ferz.features.main.bottomSheet.view_holder.RightPanelViewHolder
import java.util.*


class BottomSheetAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var callPOIFeed: ArrayList<BottomSheetDataModel> = ArrayList()
    private var data: String? =null

    fun setcallPOIFeed(callPOIFeed: ArrayList<BottomSheetDataModel>) {
        this.callPOIFeed = callPOIFeed
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewType = holder.itemViewType
        when (viewType) {

            OPTION_MORE_TYPE -> {
                val data = callPOIFeed[position].data as String

//                (holder as RightPanelViewHolder).showData(data)
            }



        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var layout = 0
        val viewHolder: RecyclerView.ViewHolder?
        when (viewType) {
            OPTION_MORE_TYPE -> {

                layout = R.layout.right_panel
                val poiOneEditView = LayoutInflater
                        .from(parent.context)
                        .inflate(layout, parent, false)
                viewHolder = RightPanelViewHolder(poiOneEditView)

            }


            else -> viewHolder = null
        }
        return viewHolder!!
    }





    override fun getItemCount(): Int {
        return callPOIFeed.size
    }




    override fun getItemViewType(position: Int): Int {
        return when {

            callPOIFeed[position].modelType.type == OPTION_MORE_TYPE -> OPTION_MORE_TYPE
            callPOIFeed[position].modelType.type == CANTACT_NOT_FIND_TYPE -> CANTACT_NOT_FIND_TYPE
            callPOIFeed[position].modelType.type == NPAY_ACOUNT_NOT_FIND_TYPE -> NPAY_ACOUNT_NOT_FIND_TYPE
            callPOIFeed[position].modelType.type == SCANNER_NOT_FIND_TYPE -> SCANNER_NOT_FIND_TYPE
            callPOIFeed[position].modelType.type == DATE_PICKER_TYPE_FROM -> DATE_PICKER_TYPE_FROM
            callPOIFeed[position].modelType.type == DATE_PICKER_TYPE_TO -> DATE_PICKER_TYPE_TO
            callPOIFeed[position].modelType.type == PAY_BILL -> PAY_BILL

            else -> -1
        }
    }

    companion object {
        private const val OPTION_MORE_TYPE = 0
        private const val CANTACT_NOT_FIND_TYPE = 1
        private const val NPAY_ACOUNT_NOT_FIND_TYPE = 2
        private const val SCANNER_NOT_FIND_TYPE = 3
        private const val DATE_PICKER_TYPE_FROM = 4
        private const val DATE_PICKER_TYPE_TO = 5
        private const val PAY_BILL = 6


    }
}

