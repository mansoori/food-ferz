package ir.food.ferz.features.home

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.androidnetworking.error.ANError
import ir.food.ferz.data.model.request.AddBascketBody
import ir.food.ferz.data.model.response.GetAllFoodResponse
import ir.food.ferz.data.model.response.GetFoodTypeResponse
import ir.food.ferz.data.model.response.VerifyResponse
import ir.food.ferz.features.base.BaseViewModel

class FoodsViewModel : BaseViewModel(), LifecycleObserver {

    // ************* Live Data
    var allFoodResult = MutableLiveData<List<GetAllFoodResponse>>()


    override fun init() {

    }

    fun getAllFood( foodTypeResponse: GetFoodTypeResponse=GetFoodTypeResponse(0,0,"","")) {
//todo fix id after server fix
        appRepository.getAllFood(
           /* foodTypeResponse.foodCategoryId.toString()*/"0", /*foodTypeResponse.id.toString()*/"0", "0", "0", "0",
            allFoodResult, isLoadingApi, errorApi
        )
    }



}
