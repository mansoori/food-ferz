package ir.food.ferz.features.home

import android.content.Intent
import android.view.View
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentHomeBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_option.*
import javax.inject.Inject


class PreviewHomeFragment : BaseFragment<FragmentHomeBinding, PreviewHomeViewModel>(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.mainMenu -> {
                (requireActivity() as MainActivity).showFragment(HomeFragment.newInstance())
            }
            R.id.makePiza -> {
              toast("در حال پیاده سازی میباشد")


            }
        }
    }

    override fun getViewModel(): PreviewHomeViewModel = previewHomeViewModel

    override fun initView(view: View) {
        mainMenu.setOnClickListener(this)
        makePiza.setOnClickListener(this)

    }

    override fun initVmObservers() {

    }

    @Inject
    lateinit var previewHomeViewModel: PreviewHomeViewModel

    override fun layout() = R.layout.fragment_option

    companion object {
        fun newInstance() = PreviewHomeFragment()
    }


}
