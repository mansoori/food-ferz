package ir.food.ferz.features.main.bottomSheet.view_holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import kotlinx.android.synthetic.main.right_panel.view.*


class RightPanelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    init {
        itemView.icHideBottomSheet.setOnClickListener(this)
        itemView.navMyAddress.setOnClickListener(this)
        itemView.navMyBascket.setOnClickListener(this)
        itemView.navProfile.setOnClickListener(this)
        itemView.navMyBookmark.setOnClickListener(this)
        itemView.navMyOrder.setOnClickListener(this)
        itemView.navExit.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.icHideBottomSheet -> {
                App.getBus().post(OttoToken(BusActionEnum.HIDE_BOTTOM_SHEET, null))
            }
            R.id.navMyAddress -> {
                App.getBus().post(OttoToken(BusActionEnum.MY_ADDRESS, null))

            }
            R.id.navMyBascket -> {
                App.getBus().post(OttoToken(BusActionEnum.MY_BASCKET, null))

            }
            R.id.navProfile -> {
                App.getBus().post(OttoToken(BusActionEnum.PROFILE, null))

            }
            R.id.navMyOrder -> {
                App.getBus().post(OttoToken(BusActionEnum.ORDERS, null))

            }
            R.id.navMyBookmark -> {
                App.getBus().post(OttoToken(BusActionEnum.BOOKMARK, null))

            }
            R.id.navExit -> {
                App.getBus().post(OttoToken(BusActionEnum.USER_EXIT, null))

            }
        }
    }


}



