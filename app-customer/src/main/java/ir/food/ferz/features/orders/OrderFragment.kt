package ir.food.ferz.features.home

import android.view.View
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentAddReviewBinding
import ir.food.ferz.features.base.BaseFragment
import javax.inject.Inject


class OrderFragment : BaseFragment<FragmentAddReviewBinding, OrderViewModel>() {


    override fun getViewModel(): OrderViewModel = orderViewModel

    override fun initView(view: View) {

    }

    override fun initVmObservers() {


    }

    @Inject
    lateinit var orderViewModel: OrderViewModel

    override fun layout() = R.layout.fragment_order

    companion object {
        fun newInstance() = OrderFragment()
    }

    fun onClick(v: View?) {

    }
}
