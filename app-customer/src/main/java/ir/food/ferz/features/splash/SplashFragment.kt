package ir.food.ferz.features.splash

import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentSplashBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.home.PreviewHomeFragment
import ir.food.ferz.features.address.map.MapFragment
import ir.food.ferz.shared.utils.AnimationUtils
import ir.food.ferz.shared.utils.extension.toGone
import ir.food.ferz.shared.utils.extension.toInVisible
import ir.food.ferz.shared.utils.extension.toVisible
import kotlinx.android.synthetic.main.fragment_splash.*
import javax.inject.Inject

class SplashFragment : BaseFragment<FragmentSplashBinding, SplashViewModel>(), View.OnClickListener {

    override fun getViewModel(): SplashViewModel = splashViewModel

    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds

    internal val mRunnable: Runnable = Runnable {
        if (getViewModel().appRepository.preferencesHelper.isAddressSet) {
            (requireActivity() as MainActivity).showFragment(PreviewHomeFragment.newInstance())

        } else {
            (requireActivity() as MainActivity).showFragment(MapFragment.newInstance())

        }
    }

    override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }

    override fun initView(view: View) {
        tryAgain.setOnClickListener(this)
        getViewModel().getBaseInfo()
    }

    override fun initVmObservers() {
        getViewModel().baseInfoResult.observe(this, Observer {
            getViewModel().appRepository.dataField.baseInfo = it

            //Initialize the Handler
            mDelayHandler = Handler()

            //Navigate with delay
            mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)


        })

        getViewModel().errorApi.observe(this, Observer {
            tryAgain.toVisible()
//            loadingImg.clearAnimation()
//            loadingImg.toInVisible()

        })

        getViewModel().isLoadingApi.observe(this, Observer {
            tryAgain.toGone()

            loadingImg.toVisible()
            AnimationUtils.rotationAnimate(requireActivity() as MainActivity, loadingImg)

        })


    }

    @Inject
    lateinit var splashViewModel: SplashViewModel

    override fun layout() = R.layout.fragment_splash

    companion object {
        fun newInstance() = SplashFragment()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tryAgain -> {
                getViewModel().getBaseInfo()

            }
        }
    }

}
