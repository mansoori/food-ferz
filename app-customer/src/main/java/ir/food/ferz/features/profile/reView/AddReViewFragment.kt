package ir.food.ferz.features.home

import android.view.View
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentAddReviewBinding
import ir.food.ferz.features.base.BaseFragment
import javax.inject.Inject


class AddReViewFragment : BaseFragment<FragmentAddReviewBinding, ProfileViewModel>() {


    override fun getViewModel(): ProfileViewModel = profileViewModel

    override fun initView(view: View) {

    }

    override fun initVmObservers() {



    }

    @Inject
    lateinit var profileViewModel: ProfileViewModel

    override fun layout() = R.layout.fragment_add_review

    companion object {
        fun newInstance() = AddReViewFragment()
    }

    fun onClick(v: View?) {

    }
}
