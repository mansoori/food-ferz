package ir.food.ferz.features.home

import android.view.View
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentAddReviewBinding
import ir.food.ferz.features.base.BaseFragment
import javax.inject.Inject


class BookmarkFragment : BaseFragment<FragmentAddReviewBinding, BookmarkViewModel>() {


    override fun getViewModel(): BookmarkViewModel = bookmarkViewModel

    override fun initView(view: View) {

    }

    override fun initVmObservers() {

    }

    @Inject
    lateinit var bookmarkViewModel: BookmarkViewModel

    override fun layout() = R.layout.fragment_bookmark

    companion object {
        fun newInstance() = BookmarkFragment()
    }

    fun onClick(v: View?) {

    }
}
