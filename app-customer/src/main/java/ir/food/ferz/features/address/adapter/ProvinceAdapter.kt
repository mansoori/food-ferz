package ir.food.ferz.features.address.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.data.model.response.GetProvinceResponse

class ProvinceAdapter(val context: Context, var listItemsTxt: List<GetProvinceResponse>) : BaseAdapter() {


    val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_spiner, parent, false)
            vh = ItemRowHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }

        // setting adapter item height programatically.

//        val params = view.layoutParams
//        params.height = 60
//        view.layoutParams = params
//        vh.label.setOnClickListener {
//            App.getBus().post(OttoToken(BusActionEnum.GET_CITY, listItemsTxt[position].id.toString()))
//        }
        vh.label.text = listItemsTxt.get(position).title

        return view
    }

    override fun getItem(position: Int): Any? {

        return null

    }

    override fun getItemId(position: Int): Long {

        return 0

    }

    override fun getCount(): Int {
        return listItemsTxt.size
    }

    private class ItemRowHolder(row: View?) {

        val label: TextView

        init {
            this.label = row?.findViewById(R.id.rowTxt) as TextView
        }
    }
}