package ir.food.ferz.features.home

import android.view.View
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentReviewBinding
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.profile.adapter.CommentAdapter
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_review.*
import javax.inject.Inject


class ReViewFragment : BaseFragment<FragmentReviewBinding, ProfileViewModel>() {


    override fun getViewModel(): ProfileViewModel = profileViewModel

    override fun initView(view: View) {
        getViewModel().getReView("0")

    }

    override fun initVmObservers() {
        getViewModel().reViewResult.observe(this, Observer {
            var reViewAdapter = CommentAdapter(activity!!, it)
            recyclerView.adapter = reViewAdapter
//
        })

    }

    @Inject
    lateinit var profileViewModel: ProfileViewModel

    override fun layout() = R.layout.fragment_review

    companion object {
        fun newInstance() = ReViewFragment()
    }

    fun onClick(v: View?) {
        when(v?.id){


        }

    }
}
