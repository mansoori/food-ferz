package ir.food.ferz.features.home

import android.view.View
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentHomeBinding
import ir.food.ferz.features.base.BaseFragment
import javax.inject.Inject


class SampleFragment : BaseFragment<FragmentHomeBinding, SampleViewModel>(),View.OnClickListener {
    override fun onClick(v: View?) {
    }
    override fun onResume() {
        super.onResume()
        App.getBus().register(this)
    }

    override fun onPause() {
        super.onPause()
        App.getBus().unregister(this)
    }

    override fun getViewModel(): SampleViewModel = homeViewModel

    override fun initView(view: View) {

    }

    override fun initVmObservers() {

    }

    @Inject
    lateinit var homeViewModel: SampleViewModel

    override fun layout() = R.layout.fragment_map

    companion object {
        fun newInstance() = SampleFragment()
    }


}
