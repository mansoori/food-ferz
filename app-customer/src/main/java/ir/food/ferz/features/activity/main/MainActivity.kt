package ir.food.ferz.features.activity.main

import androidx.appcompat.app.AppCompatActivity
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.squareup.otto.Subscribe
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.databinding.ActivityMainBinding
import ir.food.ferz.features.address.map.MapFragment
import ir.food.ferz.features.base.BaseActivity
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.bottomSheet.enums.BottomSheetAdapterType
import ir.food.ferz.features.home.*
import ir.food.ferz.features.login.LoginFragment
import ir.food.ferz.features.splash.SplashFragment
import ir.food.ferz.shared.listener.PermisionListener
import ir.food.ferz.shared.utils.extension.isAddToBackStack
import ir.food.ferz.features.main.bottomSheet.BottomSheetFragment
import ir.food.ferz.shared.utils.DialogUtils
import ir.food.ferz.shared.utils.extension.toGone
import ir.food.ferz.shared.utils.extension.toVisible
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject


class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.toolbar_iv_back -> onBackPressed()

        }
    }

    @Inject
    lateinit var mainViewModel: MainViewModel

    override fun getViewModel(): MainViewModel = mainViewModel

    @Inject
    lateinit var dialogUtils: DialogUtils


    override fun initView(view: View) {
        toolbar_iv_back.setOnClickListener(this)
        dialogUtils.init(this)
        if (getViewModel().appRepository.preferencesHelper.isLogin)
            showFragment(SplashFragment.newInstance())
        else
            showFragment(LoginFragment.newInstance())

    }

    override fun initVmObservers() {

    }


    override fun layout() = R.layout.activity_main
    override fun fragmentContainer() = R.id.container

    override fun onResume() {
        super.onResume()
        App.getBus().register(this)
    }

    override fun onPause() {
        super.onPause()
        App.getBus().unregister(this)
    }


    private var currentFragment: Fragment? = null
    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        currentFragment = fragment
        Handler().postDelayed({
            handleViewToolbar(getCurrentFragment())

        }, 10)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {

        }
    }

    fun getPermission(permission: String, permisionListener: PermisionListener) {
        Dexter.withActivity(this@MainActivity)
            .withPermission(permission)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    permisionListener.onGranted()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    if (response!!.isPermanentlyDenied) {

                        showPermissionNeverAsk(permission)
                    } else
                        permisionListener.onNeverAsk()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {

                    token?.continuePermissionRequest()
                }
            }).check()
    }


    fun showPermissionNeverAsk(permission: String) {


/*
        if (permission.contains("READ_CONTACTS")) {
            AlertDialog.Builder(this)
                .setMessage(getText(R.string.contact_permission))
                .setCancelable(false)
                .setPositiveButton(
                    getText(R.string.settings)
                ) { _, _ ->
                    CommonUtil.openAppSettings(this)
                }
                .setNegativeButton(getText(R.string.cancel), null)
                .show()
        }
        if (permission.contains("WRITE_EXTERNAL_STORAGE")) {
            AlertDialog.Builder(this)
                .setMessage(getText(R.string.storage_permission))
                .setCancelable(false)
                .setPositiveButton(
                    getText(R.string.settings)
                ) { _, _ ->
                    CommonUtil.openAppSettings(this)
                }
                .setNegativeButton(getText(R.string.cancel), null)
                .show()
        }
*/


    }

    override fun onBackPressed() {


        if (supportFragmentManager.backStackEntryCount != 0
        ) {

            if (getCurrentFragment() is LoginFragment || getCurrentFragment() is PreviewHomeFragment
                || getCurrentFragment() is MapFragment && !getViewModel().appRepository.preferencesHelper.isAddressSet
                || getCurrentFragment() is SplashFragment

            ) {
                setResult(AppCompatActivity.RESULT_CANCELED)
                finishAffinity()

            } else {

                supportFragmentManager.popBackStack()
                Handler().postDelayed({
                    handleViewToolbar(getCurrentFragment())
                }, 10)

            }


        } else {
            setResult(AppCompatActivity.RESULT_CANCELED)
            finishAffinity()

        }

    }

    @Subscribe
    fun answerAvailable(event: OttoToken) {
        Timber.d("answerAvailable : " + event.action?.name.toString())
        when (event.action) {
            BusActionEnum.HIDE_KEYBOARD -> hideSoftKeyboard(this)
            BusActionEnum.HIDE_BOTTOM_SHEET -> hideBottomSheet()
            BusActionEnum.SHOW_KEYBOARD -> {
            }
            BusActionEnum.MY_ADDRESS -> {
                hideBottomSheet()
                showFragment(MyAddressFragment.newInstance())
            }
            BusActionEnum.MY_BASCKET -> {
                hideBottomSheet()
                showFragment(BascketFragment.newInstance())
            }
            BusActionEnum.PROFILE -> {
                hideBottomSheet()
                showFragment(ProfileFragment.newInstance())

            }
            BusActionEnum.ORDERS -> {
                hideBottomSheet()
                showFragment(OrderFragment.newInstance())

            }
            BusActionEnum.BOOKMARK -> {
                hideBottomSheet()
                showFragment(BookmarkFragment.newInstance())
            }
            BusActionEnum.USER_EXIT -> {
                hideBottomSheet()
                getViewModel().appRepository.preferencesHelper.isLogin = false
                showFragment(LoginFragment.newInstance())
            }
        }
    }

    private lateinit var bottomSheetDialog: BottomSheetFragment

    fun showBottomSheet(type: BottomSheetAdapterType, data: Any?) {
//        isShowBottomSheet = true
//        behavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED


        bottomSheetDialog = BottomSheetFragment.newInstance(type, data)


        if (!bottomSheetDialog.isAdded) {
            bottomSheetDialog.show(supportFragmentManager, "")

        } else {
            bottomSheetDialog.dismiss()
            bottomSheetDialog.show(supportFragmentManager, "")

        }
//

//        bottomSheetDialog.isCancelable = type != BottomSheetAdapterType.SCANNER_NOT_FIND_TYPE


    }

    fun hideBottomSheet() {
//        isShowBottomSheet = false
//        behavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetDialog.dismiss()

    }

    fun hideSoftKeyboard(activity: AppCompatActivity) {

        if (activity.currentFocus == null) {
            return
        }
        val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
    }

    fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.container)
    }

    fun showFragment(fragment: BaseFragment<*, *>) {

        val ft = supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment, fragment.TAG)
            .isAddToBackStack(addToStack = (fragment is SplashFragment).not())

            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, 0, 0)

        if (!supportFragmentManager.isStateSaved) {
            ft.commit()
        } else if (true) {
            ft.commitAllowingStateLoss()
        }

    }

    fun customeToolbarText(title: String) {
        toolbar_title.text = title
    }

    private fun handleViewToolbar(fragment: Fragment?) {
        when (fragment?.tag) {

            "HomeFragment" -> {
                toolbar.toGone()
            }
            "LoginFragment" -> {
                toolbar.toGone()
            }
            "VerifyFragment" -> {
                toolbar.toGone()

            }
            "SplashFragment" -> {
                toolbar.toGone()
            }
            "PreviewHomeFragment" -> {
                toolbar.toGone()
            }
            "AddAddressFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "افزوردن آدرس"
            }
            "MapFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "افزودن آدرس"

            }
            "BascketFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "سبد خرید"

            }

            "ProfileFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "حساب کاربری"

            }
            "DiscountFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "جایزه و تخفیف"

            }
            "AddAddressFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "افزایش موجودی"

            }
            "ReViewFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "نظرات شما"

            }
            "MyAddressFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "آدرس های من"

            }
            "FoodsFragment" -> {
                toolbar.toVisible()
            }
            "OrderFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "سفارشات"

            }
            "BookmarkFragment" -> {
                toolbar.toVisible()
                toolbar_title.text = "دخیره شده ها"

            }
        }
    }
}

