package ir.food.ferz.features.home

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.androidnetworking.error.ANError
import ir.food.ferz.features.base.BaseViewModel

class SampleViewModel  : BaseViewModel(), LifecycleObserver {

    // ************* Live Data
    var liveIsLogin = MutableLiveData<Boolean>()
    var appSetting = MutableLiveData<ANError>()


    fun getIsLogin() : LiveData<Boolean>{
        return liveIsLogin;
    }
    // ************* Live Data

   override fun init() {

    }



}
