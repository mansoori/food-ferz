package ir.food.ferz.features.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.moeidbannerlibrary.R;
import com.example.moeidbannerlibrary.banner.BannerLayout;
import ir.food.ferz.data.model.response.FoodsCategorys;
import ir.food.ferz.shared.utils.AppConstantsKt;
import ir.food.ferz.shared.utils.CommonUtil;
import org.w3c.dom.Text;

import java.util.List;

import static ir.food.ferz.shared.utils.AppConstantsKt.IMAGE_URL;

/**
 * Created by test on 2017/11/22.
 */


public class BaseBannerAdapter extends RecyclerView.Adapter<BaseBannerAdapter.MzViewHolder> {

    private Context context;
    private List<FoodsCategorys> urlList;
    private BannerLayout.OnBannerItemClickListener onBannerItemClickListener;
    private BannerLayout.OnBannerSelectedListener onBannerSelectedListener;

    public BaseBannerAdapter(Context context, List<FoodsCategorys> urlList) {
        this.context = context;
        this.urlList = urlList;
    }

    public void setOnBannerItemClickListener(BannerLayout.OnBannerItemClickListener onBannerItemClickListener) {
        this.onBannerItemClickListener = onBannerItemClickListener;
    }

    public void setOnBannerSelectedListener(BannerLayout.OnBannerSelectedListener onBannerSelectedListener1) {
        this.onBannerSelectedListener = onBannerSelectedListener1;
    }

    @Override
    public MzViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MzViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.banner_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MzViewHolder holder, final int position) {
        if (urlList == null || urlList.isEmpty())
            return;
        final int P = position % urlList.size();
        String url =urlList.get(P).getImageName();
        ImageView img = (ImageView) holder.imageView;
        TextView txtTitle = (TextView) holder.textView;
//        Glide.with(context).load(url).into(img);
        CommonUtil.Companion.generateImage(url,img,context,R.drawable.bg_transparent);
        txtTitle.setText(urlList.get(P).getName());
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBannerItemClickListener != null) {
                    onBannerItemClickListener.onItemClick(P);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        if (urlList != null) {
            return urlList.size();
        }
        return 0;
    }


    class MzViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        MzViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            textView = (TextView) itemView.findViewById(R.id.titleFood);
        }
    }

}
