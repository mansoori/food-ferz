package ir.food.ferz.features.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import dagger.android.support.DaggerAppCompatActivity
import ir.food.ferz.BR
import ir.food.ferz.R
import ir.food.ferz.data.ErrorHandler.HandleError
import ir.food.ferz.data.ErrorHandler.OnHandleErrorCallback
import ir.food.ferz.shared.utils.DEFAULT_TYPEFACE
import ir.food.ferz.data.repository.AppRepository
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import javax.inject.Inject


abstract class BaseActivity<VDB: ViewDataBinding, VM: BaseViewModel> : DaggerAppCompatActivity() {
    private var defaultErrorObserver: Observer<Throwable>? = null

    private lateinit var mViewDataBinding: VDB
    private var mViewModel: VM? = null
    private lateinit var context: Context

    @LayoutRes
    protected abstract fun layout(): Int
    abstract fun getViewModel(): VM
    protected abstract fun initView(view: View)
    protected abstract fun initVmObservers()
    fun getViewDataBinding() = mViewDataBinding

    @Inject
    lateinit var appRepository: AppRepository

    @IdRes
    abstract fun fragmentContainer(): Int

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        performDataBinding()

        initView(mViewDataBinding.root)

        getViewModel().also {
            lifecycle.addObserver(it)
            it.init()
            defaultErrorObserver = Observer {
                it?.let {
                    HandleError(it, object : OnHandleErrorCallback(this) {
                    })
                }
            }
            it.errorApi.observe(this,defaultErrorObserver!!)

        }
        initVmObservers()

        context = this

        CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                .setDefaultFontPath(DEFAULT_TYPEFACE)
                .setFontAttrId(R.attr.fontPath)
                .build()
        )


    }


    private fun performDataBinding() {
        this.mViewModel = if (mViewModel == null) getViewModel() else mViewModel

        mViewModel?.activityLifecycleOwner = this
        mViewDataBinding = DataBindingUtil.setContentView(this, layout())
        mViewDataBinding.setVariable(BR.vm, mViewModel)
        mViewDataBinding.setVariable(BR.activity, this)
        mViewDataBinding.executePendingBindings()
    }

    fun getErrorHandler(throwable: Throwable?, errHandler : OnHandleErrorCallback){
        throwable?.let {
            getViewModel().errorApi.removeObserver(defaultErrorObserver!!)
            HandleError(it,errHandler)
        }
    }

    fun replaceFragmentSafely(
        fragment: Fragment,
        tag: String,
        allowStateLoss: Boolean = false,
        addToStack: Boolean = true,
        @AnimRes enterAnimation: Int = R.anim.fade_in,
        @AnimRes exitAnimation: Int = R.anim.fade_out,
        @AnimRes popEnterAnimation: Int = 0,
        @AnimRes popExitAnimation: Int = 0
    ) {
        val ft = supportFragmentManager.beginTransaction()

        ft.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation)
            .replace(fragmentContainer(), fragment, tag)
            .addToBackStack(tag)

        if (!supportFragmentManager.isStateSaved) {
            ft.commit()
        } else if (allowStateLoss) {
            ft.commitAllowingStateLoss()
        }
    }
}