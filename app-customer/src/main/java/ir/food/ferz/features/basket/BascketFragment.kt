package ir.food.ferz.features.home

import android.view.View
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.data.model.request.AddBascketBody
import ir.food.ferz.databinding.FragmentBascketBinding
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.basket.adapter.BascketAdapter
import ir.food.ferz.shared.utils.extension.toGone
import ir.food.ferz.shared.utils.extension.toVisible
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_bascket.*
import kotlinx.android.synthetic.main.row_bascket_food.*
import javax.inject.Inject

//todo show dialog for befor delete
class BascketFragment : BaseFragment<FragmentBascketBinding, BascketViewModel>(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.continuBuy -> {
                var addBascketBody = AddBascketBody(
                    getViewModel().appRepository.preferencesHelper.userId,
                    getViewModel().appRepository.dataField.listFood,
                    0,
                    0
                )

                getViewModel().addBascket(addBascketBody)
                toast("مرحله پرداخت در حال پیاده سازی میباشد")
            }

        }

    }

    override fun getViewModel(): BascketViewModel = bascketViewModel

    override fun initView(view: View) {
//        getViewModel().getBascket()
        continuBuy.setOnClickListener(this)
        if (getViewModel().appRepository.dataField.listFoodLocal.size > 0) {
            var bascketAdapter = BascketAdapter(
                activity!!,
                getViewModel().appRepository.dataField,
                object : BascketAdapter.DeleteFromBascketCallBack {
                    override fun onCompleteDelete() {

                        content_empty_bascket.toVisible()
                        continuBuy.toGone()

                    }


                })
            recyclerView.adapter = bascketAdapter

        } else {
            content_empty_bascket.toVisible()
            continuBuy.toGone()

        }

    }

    override fun initVmObservers() {
//        getViewModel().getBascketResult.observe(this, Observer {
//        })
        getViewModel().addBascketResult.observe(this, Observer {
            toast(it.des)
        })
    }

    @Inject
    lateinit var bascketViewModel: BascketViewModel

    override fun layout() = R.layout.fragment_bascket

    companion object {
        fun newInstance() = BascketFragment()
    }


}
