package ir.food.ferz.features.home

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.food.ferz.data.model.request.UpdateUserBody
import ir.food.ferz.data.model.response.ReViewResponse
import ir.food.ferz.data.model.response.VerifyResponse
import ir.food.ferz.features.base.BaseViewModel

class ProfileViewModel : BaseViewModel(), LifecycleObserver {

    // ************* Live Data
    var liveIsLogin = MutableLiveData<Boolean>()
    var userUpdateResult = MutableLiveData<VerifyResponse>()
    var reViewResult = MutableLiveData<List<ReViewResponse>>()
    var verifyDiscountResult = MutableLiveData<VerifyResponse>()
    var addCreditResult = MutableLiveData<VerifyResponse>()


    fun getIsLogin(): LiveData<Boolean> {
        return liveIsLogin;
    }
    // ************* Live Data

    override fun init() {

    }

    fun getUserUpdate(userBody: UpdateUserBody) {
        appRepository.getUserUpdate(userBody, userUpdateResult, isLoadingApi, errorApi)
    }

    fun getReView(index: String) {
        appRepository.getReView(index, reViewResult, isLoadingApi, errorApi)
    }

    fun getDiscount(userId: String, discountCode: String) {

        appRepository.getverfiyDiscount(userId, discountCode, verifyDiscountResult, isLoadingApi, errorApi)
    }

    fun getAddCredit(userId: String, credit: String) {
        appRepository.getaddCredits(userId, credit, addCreditResult, isLoadingApi, errorApi)
    }
}
