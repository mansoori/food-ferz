package ir.food.ferz.features.food.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.data.model.response.GetFoodTypeResponse
import ir.food.ferz.data.model.response.ReViewResponse
import ir.food.ferz.shared.utils.CommonUtil

class FoodTypeAdapter(val mContext: Context, val listItem: List<GetFoodTypeResponse>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as ViewHolderRow)?.bindItem(position, listItem)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater
            .from(mContext)
            .inflate(R.layout.row_food_type, parent, false)

        return ViewHolderRow(view)


    }

    override fun getItemCount(): Int {

        return listItem.size
    }


    class ViewHolderRow(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(position: Int, itemList: List<GetFoodTypeResponse>) {

            val item = itemList[position]

            val rowTitleTxt = itemView.findViewById<TextView>(R.id.titleFoodType)
            val imageView = itemView.findViewById<ImageView>(R.id.imgFoodType)

            rowTitleTxt.text = item.name
            CommonUtil.generateImage(item.imageName, imageView, itemView.context)
            itemView.setOnClickListener {
                App.getBus().post(OttoToken(BusActionEnum.GET_ALL_FOOD, item))
            }


        }

    }
}

