package ir.food.ferz.features.basket.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.R
import ir.food.ferz.data.model.response.GetAllFoodResponse
import ir.food.ferz.data.model.response.GetBascketResponse
import ir.food.ferz.data.model.response.ReViewResponse
import ir.food.ferz.data.repository.DataField
import ir.food.ferz.features.WidgetBuyBascket
import ir.food.ferz.features.callBack.BuyBascketClickCallBack
import ir.food.ferz.shared.utils.CommonUtil
import ir.food.ferz.shared.utils.DialogUtils
import kotlinx.android.synthetic.main.add_bascket_view.view.*

class BascketAdapter(
    val mContext: Context,
    private val dataField: DataField,
    private val deletetCallBack: DeleteFromBascketCallBack
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderRow)?.bindItem(dataField, position, this, deletetCallBack)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater
            .from(mContext)
            .inflate(R.layout.row_bascket_food, parent, false)

        return ViewHolderRow(view)


    }

    override fun getItemCount(): Int {

        return dataField.listFoodLocal.size
    }

    interface DeleteFromBascketCallBack {
        fun onCompleteDelete()
    }

    class ViewHolderRow(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItem(
            dataField: DataField,
            position: Int,
            adapter: BascketAdapter,
            deleteCallBack: DeleteFromBascketCallBack
        ) {

            val item = dataField.listFoodLocal[position]

            val nameFood = itemView.findViewById(R.id.nameFood) as TextView
            val contentFood = itemView.findViewById(R.id.contentFood) as TextView
            val priceFood = itemView.findViewById(R.id.priceFood) as TextView
            val imgFood = itemView.findViewById(R.id.imgFood) as ImageView
            val deleteFromBascket = itemView.findViewById(R.id.deleteFromBascket) as ImageView
            val addToBascket = itemView.findViewById(R.id.widgetAddToBascket) as WidgetBuyBascket


            nameFood.text = item.name
            contentFood.text = item.description
            addToBascket.setCount(item.count.toString())
            priceFood.text = item.price.toString() + "تومان"
            CommonUtil.generateImage(item.imageName, imgFood, itemView.context,R.drawable.ic_empty_food)


            deleteFromBascket.setOnClickListener {
                dataField.listFoodLocal.remove(item)
                adapter.notifyDataSetChanged()
                if (dataField.listFoodLocal.size==0){
                    deleteCallBack.onCompleteDelete()
                }

            }

            addToBascket.setOnTochClick(object : BuyBascketClickCallBack {
                override fun onToch() {
                    DialogUtils().showDialogAddBascket(itemView.context, addToBascket.countOrder, item, dataField)
                }
            })

        }

    }

}

