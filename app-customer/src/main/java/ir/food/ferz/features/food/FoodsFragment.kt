package ir.food.ferz.features.home

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentFoodsBinding
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.food.adapter.FoodTypeAdapter
import ir.food.ferz.features.food.adapter.SubFoodTypeAdapter
import kotlinx.android.synthetic.main.fragment_foods.*
import javax.inject.Inject
import android.os.Bundle
import androidx.lifecycle.Observer
import com.squareup.otto.Subscribe
import ir.food.ferz.App
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.data.model.request.AddBascketBody
import ir.food.ferz.data.model.request.FoodsJson
import ir.food.ferz.data.model.response.GetAllFoodResponse
import ir.food.ferz.data.model.response.GetBascketResponse
import ir.food.ferz.data.model.response.GetFoodTypeResponse
import ir.food.ferz.data.room.DatabaseCallback
import ir.food.ferz.data.room.FoodStorged
import ir.food.ferz.data.room.LocalCacheManager
import ir.food.ferz.data.utils.database.AppDatabase
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.shared.utils.extension.toVisible
import ir.food.ferz.shared.utils.extension.toast
import timber.log.Timber

//todo fix scal animation
//todo page ingenation
//todo fix lose data when return this page
//add review
//add tag
class FoodsFragment : BaseFragment<FragmentFoodsBinding, FoodsViewModel>(), View.OnClickListener, DatabaseCallback {
    override fun onFoodsLoaded(foodStorgeds: MutableList<FoodStorged>?) {
        toast("food loaded")
    }

    override fun onFoodsDeleted() {
        toast("food deleted")
    }

    override fun onFoodsAdded() {
        toast("food adede")
    }

    override fun onDataNotAvailable() {
        toast("food not available")
    }

    override fun onFoodUpdated() {
        toast("food food update")
    }


//    @Inject
//    lateinit var localCacheManager: LocalCacheManager

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.visitBascket -> (requireActivity() as MainActivity).showFragment(BascketFragment.newInstance())

        }

    }

    override fun getViewModel(): FoodsViewModel = foodViewModel

    override fun initView(view: View) {
        val bundle = this.arguments
        val obj = bundle!!.getSerializable("your_obj") as List<GetFoodTypeResponse>
        (requireActivity() as MainActivity).customeToolbarText(bundle!!.getString("title") as String)
        var foodTypeAdapter = FoodTypeAdapter(activity!!, obj)

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        val layoutManager1 = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        foodTypeRecycler.layoutManager = layoutManager
        subFoodTypeRecycler.layoutManager = layoutManager1
        foodTypeRecycler.adapter = foodTypeAdapter

        getViewModel().getAllFood()
        visitBascket.setOnClickListener(this)
    }

    override fun initVmObservers() {
        getViewModel().allFoodResult.observe(this, Observer {

            var foodSubTypeAdapter =
                SubFoodTypeAdapter(activity!!, it, getViewModel().appRepository.dataField)
            subFoodTypeRecycler.adapter = foodSubTypeAdapter

        })

    }

    @Inject
    lateinit var foodViewModel: FoodsViewModel

    override fun layout() = R.layout.fragment_foods

    companion object {
        fun newInstance() = FoodsFragment()
    }

    override fun onResume() {
        super.onResume()
        App.getBus().register(this)
    }

    override fun onPause() {
        super.onPause()
        App.getBus().unregister(this)
    }


    @Subscribe
    fun answerAvailable(event: OttoToken) {
        Timber.d("answerAvailable : " + event.action?.name.toString())
        when (event.action) {
            BusActionEnum.GET_ALL_FOOD -> {
                getViewModel().getAllFood(event.obj as GetFoodTypeResponse)
            }
            BusActionEnum.ADD_BASCKET -> {

                var item = event.obj as FoodStorged
//
//                var foodJson = FoodsJson(item.id, item.count, item.isPizza)
//
//                if (!getViewModel().appRepository.dataField.listFood.contains(foodJson))
//                    getViewModel().appRepository.dataField.listFood?.add(foodJson)

                LocalCacheManager.getInstance(activity!!).addFood(item as FoodStorged, this)
//
//                if (!getViewModel().appRepository.dataField.listFoodLocal.contains(item))
//                    getViewModel().appRepository.dataField.listFoodLocal?.add(item)
//

            }
        }
    }

}
