package ir.food.ferz.features.splash

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.androidnetworking.error.ANError
import ir.food.ferz.data.model.response.BaseInfoResponse
import ir.food.ferz.features.base.BaseViewModel

class SplashViewModel : BaseViewModel(), LifecycleObserver {

    // ************* Live Data
    var liveIsLogin = MutableLiveData<Boolean>()
    var appSetting = MutableLiveData<ANError>()
    var baseInfoResult = MutableLiveData<BaseInfoResponse>()


    fun getIsLogin(): LiveData<Boolean> {
        return liveIsLogin
    }

    // ************* Live Data

    override fun init() {

    }

    fun getBaseInfo() {
        appRepository.getBaseInfo(baseInfoResult, isLoadingApi, errorApi)
    }

}
