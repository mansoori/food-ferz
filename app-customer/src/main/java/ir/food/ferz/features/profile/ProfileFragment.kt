package ir.food.ferz.features.home

import android.view.View
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.data.model.request.UpdateUserBody
import ir.food.ferz.databinding.FragmentProfileBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_credit.*
import kotlinx.android.synthetic.main.fragment_profile.*
import javax.inject.Inject


class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>() {
    fun onClick(v: View?) {
        when (v?.id) {

            R.id.btnDone -> {
                if (edtName.text.toString().isNotEmpty()) {
                    var updateUserBody = UpdateUserBody(
                        0,
                        edtName.text.toString(),
                        edtfName.text.toString(),
                        "",
                        edtReagentCode.text.toString()
                    )
                    getViewModel().getUserUpdate(updateUserBody)
                }else{
                    edtName.error = "نام خود را وارد نمایید."
                }
            }
            R.id.btnDisCount -> (requireActivity() as MainActivity).showFragment(DiscountFragment.newInstance())
            R.id.btnIncreaseCredit -> (requireActivity() as MainActivity).showFragment(AddCreditsFragment.newInstance())
//            R.id.btnReView -> (requireActivity() as MainActivity).showFragment(ReViewFragment.newInstance())

        }

    }

    override fun getViewModel(): ProfileViewModel = profileViewModel

    override fun initView(view: View) {

    }

    override fun initVmObservers() {
        getViewModel().userUpdateResult.observe(this, Observer {
            toast(it.des)

        })

    }

    @Inject
    lateinit var profileViewModel: ProfileViewModel

    override fun layout() = R.layout.fragment_profile

    companion object {
        fun newInstance() = ProfileFragment()
    }


}
