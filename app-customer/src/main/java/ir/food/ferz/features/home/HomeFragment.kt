package ir.food.ferz.features.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.moeidbannerlibrary.banner.BannerLayout
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentHomeBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.bottomSheet.enums.BottomSheetAdapterType
import kotlinx.android.synthetic.main.fragment_home.*
import java.io.Serializable
import javax.inject.Inject


class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), View.OnClickListener {
    lateinit var title:String
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.toolbar_menu -> {

                (requireActivity() as MainActivity).showBottomSheet(BottomSheetAdapterType.OPTION_MORE_TYPE, "")
            }

        }
    }

    override fun getViewModel(): HomeViewModel = homeViewModel

    override fun initView(view: View) {
        val banner = view.findViewById(R.id.Banner) as BannerLayout


        val webBannerAdapter =
            BaseBannerAdapter(activity, getViewModel().appRepository.dataField.baseInfo?.foodsCategorys)
        webBannerAdapter.setOnBannerItemClickListener {
//            getViewModel().getFoodType(getViewModel().appRepository.dataField.baseInfo?.foodsCategorys!![it]!!.id.toString())
           //todo fix this id
            title=getViewModel().appRepository.dataField.baseInfo?.foodsCategorys!![it]!!.name
            getViewModel().getFoodType("0")

        }
        banner.setAdapter(webBannerAdapter)



        toolbar_menu.setOnClickListener(this)

    }

    override fun initVmObservers() {
        getViewModel().getFoodTypeResult.observe(this, Observer {
            var fragment = FoodsFragment.newInstance()
            val bundle = Bundle()
            val obj = it
            bundle.putSerializable("your_obj", obj as Serializable)
            bundle.putString("title",title )
            fragment.setArguments(bundle)



            (requireActivity() as MainActivity).showFragment(fragment)

        })
    }

    @Inject
    lateinit var homeViewModel: HomeViewModel

    override fun layout() = R.layout.fragment_home

    companion object {
        fun newInstance() = HomeFragment()
    }


}
