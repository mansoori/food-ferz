package ir.food.ferz.features.main.bottomSheet.data

import ir.food.ferz.features.bottomSheet.enums.BottomSheetAdapterType


data class BottomSheetDataModel internal constructor(
        var modelType: BottomSheetAdapterType,
        var data: Any)
