package ir.food.ferz.features.home

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.androidnetworking.error.ANError
import ir.food.ferz.data.model.request.AddBascketBody
import ir.food.ferz.data.model.response.GetBascketResponse
import ir.food.ferz.data.model.response.VerifyResponse
import ir.food.ferz.features.base.BaseViewModel

class BascketViewModel : BaseViewModel(), LifecycleObserver {

    // ************* Live Data
    var getBascketResult = MutableLiveData<List<GetBascketResponse>>()

    var addBascketResult = MutableLiveData<VerifyResponse>()


    // ************* Live Data

    override fun init() {

    }

    fun getBascket() {
//        appRepository.getBascket(
//            appRepository.preferencesHelper.userId.toString(),
//            getBascketResult,
//            isLoadingApi,
//            errorApi
//        )
    }
    fun addBascket(addBascketBody: AddBascketBody) {
        appRepository.getAddBascket(addBascketBody, addBascketResult, isLoadingApi, errorApi)

    }

}
