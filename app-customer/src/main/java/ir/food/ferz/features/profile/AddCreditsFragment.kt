package ir.food.ferz.features.home

import android.view.View
import android.widget.Button
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentCreditBinding
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_credit.*
import javax.inject.Inject


class AddCreditsFragment : BaseFragment<FragmentCreditBinding, ProfileViewModel>() {


    override fun getViewModel(): ProfileViewModel = profileViewModel

    override fun initView(view: View) {

    }

    override fun initVmObservers() {
        getViewModel().addCreditResult.observe(this, Observer {
            toast(it.des)
        })

    }

    @Inject
    lateinit var profileViewModel: ProfileViewModel

    override fun layout() = R.layout.fragment_credit

    companion object {
        fun newInstance() = AddCreditsFragment()
    }

    fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvTenthousand -> edtCridet.setText("10000")

            R.id.tvTwentyThousand -> edtCridet.setText("20000")

            R.id.tvThirtyThousand -> edtCridet.setText("30000")

            R.id.tvFortyThousand -> edtCridet.setText("40000")


            R.id.btnDone -> {
                if (edtCridet.text.toString().isNotEmpty()) {
                    getViewModel().getAddCredit(
                        getViewModel().appRepository.preferencesHelper.userId.toString(),
                        edtCridet.text.toString()
                    )
                }else{
                    edtCridet.error = "مبلغ را وارد نمایید."
                }
            }


        }
    }
}
