package ir.food.ferz.features.base

import androidx.lifecycle.*
import ir.food.ferz.App
import ir.food.ferz.data.repository.AppRepository
import javax.inject.Inject


/**
 * Base ViewModel class with default Failure handling.
 * @see ViewModel
 * @see Failure
 */


/**
 * Base ViewModel class with default Failure handling.
 * @see ViewModel
 * @see Failure
 */

abstract class BaseViewModel : ViewModel(), LifecycleObserver {
    lateinit var owner: LifecycleOwner

    lateinit var fragmentLifecycleOwner: LifecycleOwner
    lateinit var viewLifecycleOwner: LifecycleOwner
    lateinit var activityLifecycleOwner: LifecycleOwner
    var errorApi = MutableLiveData<Throwable>()
    val isLoadingApi = MutableLiveData<Boolean>()



    @Inject
    lateinit var appRepository: AppRepository


    abstract fun init()

    init {
        App.appComponent.inject(this)
    }
}