package ir.food.ferz.features.home

import android.os.CountDownTimer
import android.view.View
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentHomeBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.login.LoginViewModel
import ir.food.ferz.features.splash.SplashFragment
import ir.food.ferz.shared.utils.ValidatorUtils
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_verify_code.*
import javax.inject.Inject

//todo fix downCounter here fragment
class VerifyFragment : BaseFragment<FragmentHomeBinding, LoginViewModel>(){
    lateinit var phoneNumber: String
    lateinit var verifyCode: String
    lateinit var timer: CountDownTimer


     fun onClick(v: View?) {

        when (v?.id) {
            R.id.loginBtn -> {
                if (!ValidatorUtils.isEmpty(inputVerifyCode.text.toString()) && inputVerifyCode.text.toString() == verifyCode)
                    getViewModel().getVerify(phoneNumber)
                else
                    toast("کد تایید صحیح نمیباشد.")
            }
            R.id.tvCounter -> {
                getViewModel().getRegister(phoneNumber)
            }
        }
    }

    override fun getViewModel(): LoginViewModel = verifyViewModel

    override fun initView(view: View) {
        val bundle = this.arguments
        phoneNumber = bundle!!.getString("phoneNumber", "***********")
        verifyCode = bundle!!.getString("verifyCode", "")
        underTitle.text = "کد ارسالی به شماره " + phoneNumber + " " + "را وارد نمایید."
        counter()

    }

    override fun initVmObservers() {
        getViewModel().verifyResult.observe(this, Observer {
            (requireActivity() as MainActivity).showFragment(SplashFragment.newInstance())

        })
//        getViewModel().registerResult.observe(this, Observer {
//            counter()
//        })

    }

    @Inject
    lateinit var verifyViewModel: LoginViewModel

    override fun layout() = R.layout.fragment_verify_code

    companion object {
        fun newInstance() = VerifyFragment()
    }

    fun counter() {
         timer = object : CountDownTimer(100000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                tvCounter.text = "" + millisUntilFinished / 1000+" "+"ثانیه"
                tvCounter.isEnabled=false

            }

            override fun onFinish() {
                tvCounter.text = "تلاش مجدد"
                tvCounter.isEnabled=true

            }
        }
        timer.start()

    }

    override fun onPause() {
        super.onPause()
        timer.cancel()

    }

}
