package ir.food.ferz.features.orders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.R

class OrderAdapter(
    val mContext: Context

) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderRow)?.bindItem(position, this)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater
            .from(mContext)
            .inflate(R.layout.row_bascket_food, parent, false)

        return ViewHolderRow(view)


    }

    override fun getItemCount(): Int {

        return 3
    }

    interface DeleteFromBascketCallBack {
        fun onCompleteDelete()
    }

    class ViewHolderRow(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItem(
            position: Int,
            adapter: OrderAdapter

        ) {


        }

    }

}

