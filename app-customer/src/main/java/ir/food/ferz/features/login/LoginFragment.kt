package ir.food.ferz.features.login

import android.view.View
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentHomeBinding
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.shared.utils.extension.toast
import javax.inject.Inject
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.home.VerifyFragment
import ir.food.ferz.shared.utils.ValidatorUtils
import kotlinx.android.synthetic.main.fragment_login.*
import android.os.Bundle

//todo fix sharepreference
class LoginFragment : BaseFragment<FragmentHomeBinding, LoginViewModel>(), View.OnClickListener {
    @Inject
    lateinit var loginViewModel: LoginViewModel


    override fun getViewModel(): LoginViewModel = loginViewModel

    override fun initView(view: View) {
//        toast("this is home")
//        getTestApi()
        senCode.setOnClickListener(this)


    }

    override fun initVmObservers() {


//        getViewModel().isLoadingApi.observe(this, Observer {
//
//            if (it)
//                dialogUtils.initLoadingDialog()
//            else
//
//                dialogUtils.hideLoading()
//
//
//        })

        getViewModel().registerResult.observe(this, Observer {
            getViewModel().appRepository.preferencesHelper.userId = it.userId

            var fragment = VerifyFragment.newInstance()
            val bundle = Bundle()
            bundle.putString("phoneNumber", inputMobileNumber.text.toString())
            bundle.putString("verifyCode", it.verifyCode)
            fragment.arguments = bundle
            (requireActivity() as MainActivity).showFragment(fragment)

        })


    }

    override fun layout() = R.layout.fragment_login

    companion object {
        fun newInstance() = LoginFragment()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.senCode -> {
                if (ValidatorUtils.isValidMobileNumber(inputMobileNumber.text.toString())) {

                    getViewModel().getRegister(inputMobileNumber.text.toString())
                } else {
                    toast("شماره همراه صحیح نمیباشد.")
                }
            }
        }
    }


}
