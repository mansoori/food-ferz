package ir.food.ferz.features.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import dagger.android.support.DaggerFragment
import ir.food.ferz.BR
import ir.food.ferz.data.ErrorHandler.HandleError
import ir.food.ferz.data.ErrorHandler.OnHandleErrorCallback
import ir.food.ferz.data.repository.AppRepository
import ir.food.ferz.shared.utils.DialogUtils
import javax.inject.Inject



abstract class BaseFragment<VDB: ViewDataBinding, VM: BaseViewModel>: DaggerFragment() {

    private lateinit var mViewDataBinding: VDB
    private var mViewModel: VM? = null
    private var defaultErrorObserver: Observer<Throwable>? = null
    private var defaultLoadingObserver: Observer<Boolean>? = null

    @LayoutRes
    protected abstract fun layout(): Int

    protected abstract fun getViewModel(): VM
    protected abstract fun initView(view: View)

    @Inject
    lateinit var appRepository: AppRepository

    @Inject
    lateinit var dialogUtils: DialogUtils

    protected open fun initDataBinding(){
        this.mViewModel = getViewModel()

        mViewModel?.viewLifecycleOwner = viewLifecycleOwner
        mViewModel?.fragmentLifecycleOwner = this
        mViewModel?.activityLifecycleOwner = activity as BaseActivity<*, *>
        mViewDataBinding.setVariable(BR.vm, mViewModel)
        mViewDataBinding.setVariable(BR.fragment, this)
        mViewDataBinding.setLifecycleOwner(viewLifecycleOwner)
        mViewDataBinding.executePendingBindings()
    }
    protected abstract fun initVmObservers()
    fun getViewDataBinding() = mViewDataBinding

    val TAG = this::class.java.simpleName

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
//        performDependencyInjection()

        setHasOptionsMenu(false)
        this.activity!!.window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN or
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
        )

        mViewDataBinding = DataBindingUtil.inflate(inflater, layout(), container, false)
        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initDataBinding()
        initView(view)
        getViewModel().also {
            viewLifecycleOwner.lifecycle.addObserver(it)
//            lifecycle.addObserver(it)
            it.init()



        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initVmObservers()
        defaultErrorObserver = Observer {
            it?.let {
                HandleError(it, object : OnHandleErrorCallback(activity!!) {
                })
            }
        }

        defaultLoadingObserver = Observer {
            it?.let {
                Log.d("dialog uuu","base fragment")

                isLoading()

            }
        }
        getViewModel().errorApi.observe(this, defaultErrorObserver!!)
        getViewModel().isLoadingApi.observe(this, defaultLoadingObserver!!)

    }

    private fun performDataBinding() {
        this.mViewModel = getViewModel()

        mViewModel?.viewLifecycleOwner = viewLifecycleOwner
        mViewModel?.fragmentLifecycleOwner = this
        mViewModel?.activityLifecycleOwner = activity as BaseActivity<*, *>
        mViewDataBinding.setVariable(BR.vm, mViewModel)
        mViewDataBinding.setVariable(BR.fragment, this)
        mViewDataBinding.setLifecycleOwner(viewLifecycleOwner)
        mViewDataBinding.executePendingBindings()
    }

//    private fun performDependencyInjection() = AndroidSupportInjection.inject(this)

    fun getErrorHandler(throwable: Throwable?, errHandler : OnHandleErrorCallback){
        throwable?.let {
            getViewModel().errorApi.removeObserver(defaultErrorObserver!!)
            HandleError(it,errHandler)
        }
    }

    fun isLoading() {
        getViewModel().isLoadingApi.observe(this, Observer {
            if (it) dialogUtils.showLoading() else DialogUtils.getInstance().hideLoading()
        })
    }

}