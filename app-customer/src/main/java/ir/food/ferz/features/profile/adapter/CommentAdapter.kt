package ir.food.ferz.features.profile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.R
import ir.food.ferz.data.model.response.ReViewResponse

class CommentAdapter (val mContext: Context, private val itemList: List<ReViewResponse>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as ViewHolderRow)?.bindItem(itemList, position)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater
            .from(mContext)
            .inflate(R.layout.row_review, parent, false)

        return ViewHolderRow(view)


    }

    override fun getItemCount(): Int {

        return itemList.size
    }


    class ViewHolderRow(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(itemList: List<ReViewResponse>, position: Int) {

            val item = itemList[position]

//            val rowSuggestTvName = itemView.findViewById(R.id.tv_poi_name) as TextView

//            rowSuggestTvDescription.text = item.addressLocality





            itemView.setOnClickListener {

            }


        }

    }
}

