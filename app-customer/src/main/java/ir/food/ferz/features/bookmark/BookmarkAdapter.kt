package ir.food.ferz.features.bookmark

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.R
import ir.food.ferz.data.model.response.GetAllFoodResponse
import ir.food.ferz.data.repository.DataField
import ir.food.ferz.features.WidgetBuyBascket
import ir.food.ferz.features.callBack.BuyBascketClickCallBack
import ir.food.ferz.shared.utils.CommonUtil
import ir.food.ferz.shared.utils.DialogUtils
import android.view.animation.AnimationUtils.loadAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.add_bascket_view.view.*

//todo fix dialog add review for food
class BookmarkAdapter(val mContext: Context, val itemList: List<GetAllFoodResponse>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as ViewHolderRow)?.bindItem(position, itemList)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater
            .from(mContext)
            .inflate(R.layout.row_sub_food, parent, false)

        return ViewHolderRow(view)


    }

    override fun getItemCount(): Int {

        return 3
    }


    class ViewHolderRow(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bindItem(position: Int, itemList: List<GetAllFoodResponse>) {

        }


}


}

