package ir.food.ferz.features

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import ir.food.ferz.R
import ir.food.ferz.features.callBack.BuyBascketClickCallBack
import ir.food.ferz.shared.utils.extension.toVisible
import kotlinx.android.synthetic.main.add_bascket_view.view.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*


class WidgetBuyBascket : LinearLayout {
    internal var type = 1


    var buyBascketClickCallBack: BuyBascketClickCallBack? = null

    constructor(context: Context?) : super(context) {
        initView(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView(context)
        handleAttrs(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context)
        handleAttrs(attrs)
    }

    fun initView(context: Context?) {
        val inflater = getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val vParent = inflater.inflate(R.layout.add_bascket_view, this, true)


        addToBascket.setOnClickListener {
            buyBascketClickCallBack?.let {
                it.onToch()

            }
        }
    }

    fun setCount(str: String) {
        countOrder.toVisible()
        countOrder.text = str
    }

    fun setOnTochClick(buyBascketClickCallBack: BuyBascketClickCallBack) {

        this.buyBascketClickCallBack = buyBascketClickCallBack
    }


    @SuppressLint("CustomViewStyleable")
    private fun handleAttrs(attrs: AttributeSet?) {

//        val ta = getContext().obtainStyledAttributes(attrs, R.styleable.NamaaEditText)
//        val n = ta.indexCount
//        for (i in 0 until n) {
//            val attr = ta.getIndex(i)
//            when (attr) {
//
//                R.styleable.NamaaEditText_hint_text -> setHint(ta.getString(attr))
//                R.styleable.NamaaEditText_edit_text_gravity -> {
//                    val gravity = ta.getInt(attr, Gravity.RIGHT)
//                    edit_text.gravity = Gravity.CENTER or gravity
//                }
//                R.styleable.NamaaEditText_edt_type_input -> {
//                    setType(ta.getInt(attr, 1))
//                }
//                R.styleable.NamaaEditText_maxEdtcharacter -> {
//                    setEditTextMaxLength(ta.getInteger(attr, 0))
//                }
//                R.styleable.NamaaEditText_isEdtSingleLine -> {
//                    if (ta.getBoolean(attr, true)) {
//                        edit_text.setSingleLine()
//                    } else {
//                        edit_text.setSingleLine(false)
//                        setMaxLine(Integer.MAX_VALUE)
//                    }
//                }
//                R.styleable.NamaaEditText_is_amount -> {
//                    isAmount = ta.getBoolean(attr, true)
//                }
//                R.styleable.NamaaEditText_isForcible -> {
//                    var isForce = ta.getBoolean(attr, false)
//                    setForcible(isForce)
//                }
//                R.styleable.NamaaEditText_isFloating -> {
//                    var isFloat = ta.getBoolean(attr, false)
//                    setFloating(isFloat)
//                }
//                else -> {
//                }
//            }
//        }

//        ta.recycle()
    }


}