package ir.food.ferz.features.address.map

object AppConfig {
    const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 500
}