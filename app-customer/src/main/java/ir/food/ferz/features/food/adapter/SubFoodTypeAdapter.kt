package ir.food.ferz.features.food.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import ir.food.ferz.R
import ir.food.ferz.data.model.response.GetAllFoodResponse
import ir.food.ferz.data.repository.DataField
import ir.food.ferz.features.WidgetBuyBascket
import ir.food.ferz.features.callBack.BuyBascketClickCallBack
import ir.food.ferz.shared.utils.CommonUtil
import ir.food.ferz.shared.utils.DialogUtils
import android.view.animation.AnimationUtils.loadAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.add_bascket_view.view.*

//todo fix dialog add review for food
class SubFoodTypeAdapter(val mContext: Context, val itemList: List<GetAllFoodResponse>, val dataField: DataField) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as ViewHolderRow)?.bindItem(position, itemList, dataField)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater
            .from(mContext)
            .inflate(R.layout.row_sub_food, parent, false)

        return ViewHolderRow(view)


    }

    override fun getItemCount(): Int {

        return itemList.size
    }


    class ViewHolderRow(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bindItem(position: Int, itemList: List<GetAllFoodResponse>, dataField: DataField) {

            val item = itemList[position]

            val name = itemView.findViewById(R.id.tvName) as TextView
            val discription = itemView.findViewById(R.id.tvDiscription) as TextView
            val price = itemView.findViewById(R.id.tvPrice) as TextView
            val addToBascket = itemView.findViewById(R.id.widgetAddToBascket) as WidgetBuyBascket
            addToBascket.setOnTochClick(object : BuyBascketClickCallBack {
                override fun onToch() {
                    DialogUtils().showDialogAddBascket(itemView.context, addToBascket.countOrder, item, dataField)
                }
            })
            val imgFood = itemView.findViewById(R.id.imgFood) as ImageView
            val addReview = itemView.findViewById(R.id.addReview) as ImageView
            val addMessage = itemView.findViewById(R.id.addMessage) as ImageView
            addReview.setOnClickListener {
                DialogUtils.getInstance().showDialodComment()
            }

            var flagTag=false
            addMessage.setOnClickListener {

               if(!flagTag){
                   addMessage.setImageResource(R.drawable.ic_tag_active)
                   flagTag=true
               }else{
                   addMessage.setImageResource(R.drawable.ic_tag_deactive)
                   flagTag=false

               }

            }
//
            if (dataField.listFoodLocal.size > 0) {
                if (dataField.listFoodLocal.contains(item))
                    addToBascket.setCount(dataField.listFoodLocal[position].count.toString())
            }

            name.text = item.name
            discription.text = item.description
            price.text = CommonUtil.getDecimalFormat(item.price)
            CommonUtil.generateImage(item.imageName, imgFood, itemView.context, R.drawable.ic_empty_food)

//            itemView.setOnClickListener {
//                if (true) {
//                    // run scale animation and make it bigger
//                    val anim = AnimationUtils.loadAnimation(itemView.context, R.anim.scale_in_tv)
//                    itemView.startAnimation(anim)
//                    anim.setFillAfter(true)
//                } else {
//                    // run scale animation and make it smaller
//                    val anim = AnimationUtils.loadAnimation(itemView.context, R.anim.scale_out_tv)
//                    itemView.startAnimation(anim)
//                    anim.setFillAfter(true)
//                }
//            }

            // bind focus listener

        }

        //        private var countNumber: Int = 0


    }


}

