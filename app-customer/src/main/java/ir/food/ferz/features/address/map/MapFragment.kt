package ir.food.ferz.features.address.map

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.PointF
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import com.mapbox.android.core.location.*
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.squareup.otto.Subscribe
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.databinding.FragmentMapBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.features.home.AddAddressFragment
import ir.food.ferz.features.home.AddressViewModel
import ir.food.ferz.features.home.PreviewHomeFragment
import ir.food.ferz.shared.listener.PermisionListener
import ir.food.ferz.shared.utils.*
import kotlinx.android.synthetic.main.dialog_permission.view.*
import kotlinx.android.synthetic.main.fragment_map.*
import timber.log.Timber
import javax.inject.Inject

//todo fix map helper here
class MapFragment : BaseFragment<FragmentMapBinding, AddressViewModel>(), View.OnClickListener {
    override fun layout(): Int = R.layout.fragment_map

    override fun getViewModel(): AddressViewModel = mapViewModel

    override fun initView(view: View) {
        rejectMap.setOnClickListener(this)
        fabGps.setOnClickListener(this)


        val bundle = this.arguments
        if (bundle != null){
            val obj = bundle!!.getSerializable("latlng") as LatLng
            animateToLocation(obj)

        }
    }

    override fun initVmObservers() {

    }

    companion object {
        fun newInstance() = MapFragment()
    }

    @Inject
    lateinit var mapViewModel: AddressViewModel

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fabGps -> {


                if (!gpsUtils.isHasGpsPermission) {
                    getPermission(true)
                } else if (gpsUtils.isHasGpsPermission && gpsUtils.isHighAccuracy(activity)) {
                    lastLatLng?.let {

                        animateToLocation(it)
                    }

                } else
                    gpsUtils.showDialogGps(activity)
            }


            R.id.rejectMap -> {
                var fragmnet = AddAddressFragment.newInstance()
                val bundle = Bundle()
                bundle.putString("lat", getCenter().latitude.toString())
                bundle.putString("lng", getCenter().longitude.toString())
                fragmnet.arguments = bundle
                (requireActivity() as MainActivity).showFragment(fragmnet)

            }

        }
    }

    private var mapStyle: Style? = null
    private var permissionsManager: PermissionsManager? = null
    private var mapboxMap: MapboxMap? = null
    private var mapView: MapView? = null

    private var lastLatLng: LatLng? = null
    private var isAnimate = false

    private var isFristMove: Boolean = true
    private var locationEngine: LocationEngine? = null
    lateinit var locationEngineRequest: LocationEngineRequest
    lateinit var gpsUtils: GpsUtils

    val callback = object : LocationEngineCallback<LocationEngineResult> {
        override fun onSuccess(locationEngineResult: LocationEngineResult?) {
            locationEngineResult?.lastLocation?.let {
                lastLatLng = LatLng(
                    it.latitude,
                    it.longitude
                )
                if (!isAnimate) {
                    isAnimate = true
                    animateToLocation(
                        lastLatLng!!
                    )
                }
            }
        }

        override fun onFailure(p0: Exception) {
        }

    }

    private fun getCenter(): LatLng =
        LatLng(mapboxMap?.cameraPosition?.target?.latitude!!, mapboxMap?.cameraPosition?.target?.longitude!!)


    private fun animateToLocation(latLng: LatLng) {

        mapboxMap?.uiSettings?.isScrollGesturesEnabled = false
        mapboxMap?.animateCamera(
            CameraUpdateFactory.newLatLngZoom(latLng, 16.0),
            1500,
            object : MapboxMap.CancelableCallback {
                override fun onFinish() {
                    mapboxMap?.uiSettings?.isScrollGesturesEnabled = true
                }

                override fun onCancel() {
                    mapboxMap?.uiSettings?.isScrollGesturesEnabled = true
                }
            })
    }

    private var dialogPermission: Dialog? = null

    private fun initDialogPermission() {
        dialogPermission = Dialog(context)
        dialogPermission?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogPermission?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogPermission?.setCancelable(false)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_permission, null)
        view.iv_cancel_permission?.setOnClickListener {
            hidePermission()
        }
        view.tv_permission_setting?.setOnClickListener {
            hidePermission()
            openAppSettings()
        }
        dialogPermission?.setContentView(view)
        dialogPermission?.show()

    }

    fun openAppSettings() {

        val packageUri = Uri.fromParts("package", activity!!.packageName, null)

        val applicationDetailsSettingsIntent = Intent()

        applicationDetailsSettingsIntent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        applicationDetailsSettingsIntent.data = packageUri
        applicationDetailsSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        activity!!.startActivity(applicationDetailsSettingsIntent)

    }

    private fun hidePermission() {
        dialogPermission?.dismiss()

    }

    private fun getPermission(showPermissionDialog: Boolean = false) {
        gpsUtils.getPermission(true, object : PermisionListener {
            override fun onNeverAsk() {
                if (showPermissionDialog)
                    initDialogPermission()
            }

            override fun onGranted() {
                initializeLocationEngine()
                initializeLocationComponent(mapStyle!!)
            }

        })
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocationComponent(style: Style) {
        val locationComponent = mapboxMap?.locationComponent
        locationComponent?.activateLocationComponent(activity!!, style, locationEngine!!)
        locationComponent?.isLocationComponentEnabled = true
        locationComponent?.renderMode = RenderMode.COMPASS

    }

    @SuppressLint("MissingPermission")
    private fun initializeLocationEngine() {
//        getViewModel().stateMachine.getState()?.input?.let { latLng ->
//            locationEngine.requestLocationUpdates(locationEngineRequest, null)
//            animateToLocation(latLng as LatLng)
//        } ?: kotlin.run {
        locationEngine?.requestLocationUpdates(locationEngineRequest, callback, null)
        goLastLocation()
//        }

    }

    @SuppressLint("MissingPermission")
    private fun goLastLocation() {
        locationEngine?.getLastLocation(callback)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(
            activity!!,
            "pk.eyJ1IjoibmFyZ2VzaSIsImEiOiJjamNkMGFzbGwwNnlmMndydnJiNng2MjB5In0.ieZHH3sKCJYoSXhy_IWdNg"
        )
        locationEngine = LocationEngineProvider.getBestLocationEngine(activity!!)
        locationEngineRequest = LocationEngineRequest.Builder(AppConfig.UPDATE_INTERVAL_IN_MILLISECONDS)
            .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
            .setFastestInterval(AppConfig.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
            .build()!!

        gpsUtils = GpsUtils.getInstance()
        gpsUtils.init(activity)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        // Inflate the layout for this fragment
        var rootView = inflater.inflate(R.layout.fragment_map, container, false)

        // This contains the MapView in XML and needs to be called after the access token is configured.


        mapView = rootView.findViewById(R.id.mapView)
        mapView!!.onCreate(savedInstanceState)



        mapView?.getMapAsync {

            mapboxMap = it
            it.setStyle(Style.LIGHT) { style ->
                mapStyle = style


                mapboxMap?.uiSettings!!.isRotateGesturesEnabled = false
                mapboxMap?.uiSettings!!.isCompassEnabled = false
//

                val iranBound = LatLngBounds.Builder()
                iranBound.include(LatLng(39.6753781, 63.3100633))
                iranBound.include(LatLng(24.3263089, 43.9833913))
                mapboxMap?.setLatLngBoundsForCameraTarget(iranBound.build())
                mapboxMap?.setMinZoomPreference(4.0)
                mapboxMap?.setMaxZoomPreference(18.99)
                getPermission()
            }


        }








        return rootView
    }


    override fun onStart() {
        super.onStart()
        mapView!!.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView!!.onResume()
        App.getBus().register(this)


    }

    override fun onPause() {
        super.onPause()
        mapView!!.onPause()
        App.getBus().unregister(this)
    }

    override fun onStop() {
        super.onStop()
        mapView!!.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView!!.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView!!.onLowMemory()
    }


    fun setEnableMapDragging(isEnable: Boolean) {
        mapboxMap?.uiSettings?.setAllGesturesEnabled(isEnable)
    }


    fun mapMoveToLocation() {
        var target = LatLng(33.8513546, 52.9725893)

        mapboxMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                target, 18.toDouble()
            )
        )

    }

    fun getMapViewBoundingBox() {
        Toast.makeText(Mapbox.getApplicationContext(), mapboxMap?.getBoundingBoxPad(), Toast.LENGTH_LONG).show()
    }

    fun MapboxMap.getBoundingBoxPad(): String {

        var pddingLeftRight = 150F
        var pddingTop = 150F
        var pddingBottom = 250F

        var northEastPoint = projection.fromScreenLocation(
            PointF(
                ScreenUtils.getResolationScreen(Mapbox.getApplicationContext()).x.toFloat() - pddingLeftRight,
                pddingTop.toFloat()
            )
        )
        var southWestPoint = projection.fromScreenLocation(
            PointF(
                pddingLeftRight.toFloat(), ScreenUtils.getResolationScreen(
                    Mapbox.getApplicationContext()
                ).y.toFloat() - pddingBottom
            )
        )

        return "${northEastPoint.latitude},${northEastPoint.longitude},${southWestPoint.latitude},${southWestPoint.longitude}"
    }

    fun addMarker() {
        var target = LatLng(33.8513546, 52.9725893)
        val markerView = mapboxMap?.addMarker(
            MarkerOptions()
                .position(target)
                .icon(
                    IconFactory
                        .getInstance(activity!!)
                        .fromResource(R.drawable.namaa_marker_default_select)
                )
        )


    }


    @Subscribe
    fun answerAvailable(event: OttoToken) {
        Timber.d("answerAvailable : " + event.action?.name.toString())
        when (event.action) {
            BusActionEnum.ADD_ADDRESS -> {

                //todo fix other api call when reject map
                (requireActivity() as MainActivity).showFragment(PreviewHomeFragment.newInstance())

            }


        }
    }
}
