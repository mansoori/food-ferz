package ir.food.ferz.features.home

import android.view.View
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.databinding.FragmentDiscountBinding
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_discount.*
import javax.inject.Inject


class DiscountFragment : BaseFragment<FragmentDiscountBinding, ProfileViewModel>() {


    override fun getViewModel(): ProfileViewModel = profileViewModel

    override fun initView(view: View) {


    }

    override fun initVmObservers() {
        getViewModel().verifyDiscountResult.observe(this, Observer {
            toast(it.des)

        })
    }

    @Inject
    lateinit var profileViewModel: ProfileViewModel

    override fun layout() = R.layout.fragment_discount

    companion object {
        fun newInstance() = DiscountFragment()
    }

    fun onClick(v: View?) {

        when (v?.id) {
            R.id.btnDone -> {
                if (edtDiscountCode.text.toString().isNotEmpty()) {

                    getViewModel().getDiscount(
                        getViewModel().appRepository.preferencesHelper.userId.toString(),
                        edtDiscountCode.text.toString()
                    )
                } else {
toast("کد تخفیف را وارد نمایید.")                }
            }


        }

    }
}
