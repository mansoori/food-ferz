package ir.food.ferz.features.login

import ir.food.ferz.features.base.BaseViewModel
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import ir.food.ferz.data.model.response.RegisterResponse
import ir.food.ferz.data.model.response.VerifyResponse
import ir.food.ferz.data.model.response.testResponse

class LoginViewModel : BaseViewModel(), LifecycleObserver {


    var registerResult = MutableLiveData<RegisterResponse>()
    var verifyResult = MutableLiveData<VerifyResponse>()


    override fun init() {


    }

    fun getRegister(mobile: String) {
        appRepository.getRegister(mobile, registerResult, isLoadingApi, errorApi)
    }
    fun getVerify(mobile:String){

        appRepository.getVerify(mobile,verifyResult,isLoadingApi,errorApi)
    }
}
