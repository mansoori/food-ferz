package ir.food.ferz.features.home

import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.Observer
import ir.food.ferz.R
import ir.food.ferz.data.model.request.AddAddressBody
import ir.food.ferz.data.model.response.GetCityResponse
import ir.food.ferz.data.model.response.GetProvinceResponse
import ir.food.ferz.databinding.FragmentAddAddressBinding
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.address.adapter.CityAdapter
import ir.food.ferz.features.address.adapter.ProvinceAdapter
import ir.food.ferz.features.base.BaseFragment
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.fragment_add_address.*
import kotlinx.android.synthetic.main.row_sub_food.*
import javax.inject.Inject


class AddAddressFragment : BaseFragment<FragmentAddAddressBinding, AddressViewModel>() {


    fun onClick(v: View?) {
        when (v?.id) {
            ir.food.ferz.R.id.btnDone -> {
                if (itemProvince!=null && itemCity!=null && edtCompleteAddress.text.toString().isNotEmpty()){
                var addAddressBody = AddAddressBody(
                    getViewModel().appRepository.preferencesHelper.userId,
                    itemProvince!!.title,
                    itemProvince!!.id.toInt(),
                    itemCity!!.title,
                    itemCity!!.id.toInt(),
                    edtCompleteAddress.text.toString(),
                    true, lat, lng
                )
                getViewModel().getAddAddress(addAddressBody)
                }else{
                    toast("اطلاعات آدرس کامل نمیباشد.")
                }
            }
        }
    }
//    @Inject
//    lateinit var dialogUtilss: DialogUtils

    override fun getViewModel(): AddressViewModel = addressViewModel
    var itemCity: GetCityResponse? = null
    lateinit var listCity: List<GetCityResponse>
    var itemProvince: GetProvinceResponse? = null
    lateinit var listProvince: List<GetProvinceResponse>

    lateinit var lat: String
    lateinit var lng: String

    override fun initView(view: View) {
        val bundle = this.arguments
        lat = bundle!!.getString("lat", "0")
        lng = bundle!!.getString("lng", "0")


        getViewModel().getProvince()

        ostanSpiner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                if (position > -1) {
                    itemProvince = listProvince[position]
                    getViewModel().getCity(listProvince[position].id.toString())
//                 toast(adapterView.getItemAtPosition(position).toString())
                }
//                else {
//                    StateID = 0
//
//                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {
//                StateID = 0
            }
        }



        citySpiner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (position > -1)
                    itemCity = listCity[position]

            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

    }

    override fun initVmObservers() {
        getViewModel().getProvinceResult.observe(this, Observer {
            listProvince = it
            var provinceAdapter = ProvinceAdapter(activity!!, it)
            ostanSpiner.adapter = provinceAdapter

        })
        getViewModel().getCityResult.observe(this, Observer {
            listCity = it
            var cityAdapter = CityAdapter(activity!!, it)
            citySpiner.adapter = cityAdapter

        })
        getViewModel().addAddressResult.observe(this, Observer {
            toast(it.des)
            (requireActivity() as MainActivity).showFragment(PreviewHomeFragment.newInstance())

        })
//        getViewModel().isLoadingApi.observe(this, Observer {
//            if (it)
//                dialogUtils.initLoadingDialog()
//            else
//                dialogUtils.hideLoading()
//        })

    }

    @Inject
    lateinit var addressViewModel: AddressViewModel

    override fun layout() = R.layout.fragment_add_address

    companion object {
        fun newInstance() = AddAddressFragment()
    }


}
