package ir.food.ferz.shared.utils;

import android.text.TextUtils;

/**
 * Created by root on 7/1/17.
 */

public class ValidatorUtils {


    public static boolean isValidMobileNumber(String number) {

        if (number.isEmpty() || !number.startsWith("09")) {
            return false;
        } else if (number.length() != 11) {
            return false;
        }
        try {
            double d = Double.parseDouble(number);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0 || str.equals("");
    }


    public static boolean isLetterAndDigit(String s) {
        String n = ".*[0-9].*";
        String a = ".*[A-Z].*";
        String b = ".*[a-z].*";
        return s.matches(n) && (s.matches(a) || s.matches(b));
    }

}
