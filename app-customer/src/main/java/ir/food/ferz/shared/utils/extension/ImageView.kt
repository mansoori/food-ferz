package ir.food.ferz.shared.utils.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy


internal fun ImageView.loadImageUrl(url: String) {
    Glide.with(this.context)
            .load(url)
            .asBitmap()
//            .placeholder(R.drawable.place_holder_pic)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(this)
}

internal fun ImageView.loadImageUrl(url: String,placeHolder:Int) {
    Glide.with(this.context)
            .load(url)
            .asBitmap()
            .placeholder(placeHolder)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(this)
}
