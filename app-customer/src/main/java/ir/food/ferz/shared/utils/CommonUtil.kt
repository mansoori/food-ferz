package ir.food.ferz.shared.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Base64
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import ir.food.ferz.R
import okhttp3.OkHttpClient
import java.io.ByteArrayOutputStream
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*


class CommonUtil {
    companion object {

        var valueForBase64: String? = null

        fun openAppSettings(context: Context) {

            val packageUri = Uri.fromParts("package", context.packageName, null)

            val applicationDetailsSettingsIntent = Intent()

            applicationDetailsSettingsIntent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            applicationDetailsSettingsIntent.data = packageUri
            applicationDetailsSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            context.startActivity(applicationDetailsSettingsIntent)

        }

        fun shareContent(shareLink: String, context: Context) {

            val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareLink)
            context.startActivity(Intent.createChooser(sharingIntent, "share_using"))

        }

        fun convertToEnglish(value: String): String {
            var newTextPlaque = value.replace("۱", "1")
                .replace("۲", "2")
                .replace("۳", "3").replace("۴", "4")
                .replace("۵", "5").replace("۶", "6")
                .replace("۷", "7").replace("۸", "8")
                .replace("۹", "9").replace("۰", "0")
            return newTextPlaque
        }


        fun callToNumber(context: Context, number: String) {
            val number_call = Uri.parse("tel:$number")
            val callIntent = Intent(Intent.ACTION_DIAL, number_call)
            context.startActivity(callIntent)
        }

        fun getDecimalFormat(pay: Int): String {
            val decim = DecimalFormat("#,###.##")
            var temp = (decim.format(pay))
            return "$temp تومان"


        }
//        fun showImageCroper(requestCode: Int, mContext: Context) {
//            val intent = CropImage.activity().setShowCropOverlay(true)   // data.getData() >> is Uri of picture picked
//                .setScaleType(CropImageView.ScaleType.CENTER_CROP)
//                .setAspectRatio(150, 150)
//                .getIntent(mContext)
//                .setAction(android.provider.MediaStore.INTENT_ACTION_VIDEO_CAMERA)
//            (mContext as Activity).startActivityForResult(intent, requestCode)
//        }

        fun setImageBase64(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int) {
            val byteArrayOS = ByteArrayOutputStream()
            image.compress(compressFormat, quality, byteArrayOS)
            valueForBase64 = "data:image/${compressFormat.name.toLowerCase()};base64,${Base64.encodeToString(
                byteArrayOS.toByteArray(),
                Base64.NO_WRAP
            )}"
        }


        fun getImageBase64(): String? {
            return valueForBase64
        }


        fun clearImageBase64() {
            valueForBase64 = ""
        }


        fun openUrlLink(context: Context, url: String) {
            try {
                val plink5 = Intent(
                    Intent.ACTION_VIEW, Uri
                        .parse(url)
                )
                context.startActivity(plink5)
            } catch (e: ActivityNotFoundException) {
            }
        }

        private fun CheckApplicationInstalled(context: Context, uri: String): Boolean {
            val pm = context.packageManager
            try {
                pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
                return true
            } catch (e: PackageManager.NameNotFoundException) {
            }

            return false
        }


         fun generateImage(
            nameImg: String,
            imageView: ImageView,
            context: Context,
            palceholder: Int = R.drawable.bg_transparent
        ) {

            val client = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val newRequest = chain.request().newBuilder()
//                        .addHeader("Cookie", cookie)
                        .build()
                    chain.proceed(newRequest)
                }
                .build()


            val picasso = Picasso.Builder(context)
                .downloader(OkHttp3Downloader(client))
                .listener { _, _, _ ->


                }
                .build()


            picasso.load(IMAGE_URL + nameImg)
                .placeholder(palceholder)

                .into(imageView, object : Callback {
                    override fun onSuccess() {
//                        DialogUtils.getInstance().hideLoading()


                    }

                    override fun onError() {
//                        DialogUtils.getInstance().hideLoading()


                    }


                })

        }

        fun setVectorForPreLollipop(textView: AutoCompleteTextView, resourceId: Int, activity: Context, position: Int) {
            val icon: Drawable?
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                icon = VectorDrawableCompat.create(
                    activity.resources, resourceId,
                    activity.theme
                )
            } else {
                icon = activity.resources.getDrawable(resourceId, activity.theme)
            }
            when (position) {
                DRAWABLE_LEFT -> textView.setCompoundDrawablesWithIntrinsicBounds(
                    icon,
                    null,
                    null,
                    null
                )

                DRAWABLE_RIGHT -> textView.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    null,
                    icon,
                    null
                )

                DRAWABLE_TOP -> textView.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    icon,
                    null,
                    null
                )

                DRAWABLE_BOTTOM -> textView.setCompoundDrawablesWithIntrinsicBounds(
                    null, null, null,
                    icon
                )
            }
        }

        private fun formatInteger(str: String): String {
            val parsed = BigDecimal(str)
            val formatter = DecimalFormat("$prefix#,###", DecimalFormatSymbols(Locale.US))
            return formatter.format(parsed)
        }

        private val prefix: String = ""

        private fun formatDecimal(str: String): String {
            if (str == ".") {
                return "$prefix."
            }
            val parsed = BigDecimal(str)
            // example pattern VND #,###.00
            val formatter = DecimalFormat(
                prefix + "#,###." + getDecimalPattern(str),
                DecimalFormatSymbols(Locale.US)
            )
            formatter.roundingMode = RoundingMode.DOWN
            return formatter.format(parsed)
        }

        private val MAX_DECIMAL = 3

        /**
         * It will return suitable pattern for format decimal
         * For example: 10.2 -> return 0 | 10.23 -> return 00, | 10.235 -> return 000
         */
        fun getDecimalPattern(str: String): String {
            val decimalCount = str.length - str.indexOf(".") - 1
            val decimalPattern = StringBuilder()
            var i = 0
            while (i < decimalCount && i < MAX_DECIMAL) {
                decimalPattern.append("0")
                i++
            }
            return decimalPattern.toString()
        }
    }

}