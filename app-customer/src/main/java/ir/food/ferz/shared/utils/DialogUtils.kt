package ir.food.ferz.shared.utils

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.widget.TextView
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.data.EventBus.BusActionEnum
import ir.food.ferz.data.EventBus.OttoToken
import ir.food.ferz.data.model.request.FoodsJson
import ir.food.ferz.data.model.response.GetAllFoodResponse
import ir.food.ferz.data.repository.DataField
import ir.food.ferz.data.room.FoodStorged
import ir.food.ferz.shared.utils.extension.toGone
import ir.food.ferz.shared.utils.extension.toVisible
import ir.food.ferz.shared.utils.extension.toast
import kotlinx.android.synthetic.main.dialog_add_bascket_view.*
import kotlinx.android.synthetic.main.dialog_complete_address.*

//todo debug to add arrayfood current
class DialogUtils {

    lateinit var context: Context


    private var dialogLoading: Dialog? = null
    private var dialogAddress: Dialog? = null


    companion object {
        private var instance: DialogUtils? = null
        fun getInstance(): DialogUtils {
            if (instance == null)
                instance = DialogUtils()
            return instance!!
        }
    }


    fun init(context: Context) {
        this.context = context
        initAddressDialog()
        initLoadingDialog()

    }

    fun initLoadingDialog() {
        Log.d("dialog uuu", "showwww")
        dialogLoading = Dialog(context)
        dialogLoading?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogLoading?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogLoading?.setCancelable(false)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null)
//        AnimationUtils.rotationAnimate(context, view.loadingImg)
        dialogLoading?.setContentView(view)
    }

    fun showLoading() {
        dialogLoading?.show()

    }

    fun initAddressDialog() {
        dialogAddress = Dialog(context)
        dialogAddress?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogAddress?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_complete_address, null)

        dialogAddress?.setContentView(view)
        dialogAddress?.btnDialogAddAddress?.setOnClickListener {
            if (dialogAddress?.completeAddress?.text.toString().isNotEmpty()) {
                dialogAddress?.dismiss()
                App.getBus().post(OttoToken(BusActionEnum.ADD_ADDRESS, dialogAddress?.completeAddress?.text.toString()))
            } else {
                toast("لطفا اطلاعات را تکمیل کنید.")
            }
        }
    }

    fun showDialodComment() {
        var dialogComment = Dialog(context)
        dialogComment?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogComment?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_add_review, null)

        dialogComment?.setContentView(view)

    }

    fun showDialodAddresOption() {
        var dialogComment = Dialog(context)
        dialogComment?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogComment?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_option_address, null)

        dialogComment?.setContentView(view)

    }

    fun showDialogAddBascket(
        context: Context,
        displayCount: TextView,
        item: GetAllFoodResponse,

        dataField: DataField
    ) {
        var dialog = Dialog(context)

        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_add_bascket_view, null)

        dialog?.setContentView(view)
        dialog.acceptValue.setOnClickListener {
            if (item.count > 0) {
                displayCount.toVisible()
                dialog.dismiss()
                var foodsStorged = FoodStorged(
                    item.id,
                    item.foodCategoryId,
                    item.foodTypeId,
                    item.name,
                    item.description,
                    item.price,
                    item.imageName,
                    item.isWeeklyFood,
                    item.isActive,
                    item.isPizza,
                    item.pizzaNum,
                    item.count
                )
                App.getBus().post(OttoToken(BusActionEnum.ADD_BASCKET, foodsStorged))

            } else {
                ir.food.ferz.shared.utils.AnimationUtils.onShakeAnimation(dialog.txtCount, context)
            }

        }
        if (item.count > 0) {
            dialog.txtCount.text = item.count.toString()
        }
        dialog.increase.setOnClickListener {
            item.count += 1
            dialog.txtCount.text = item.count.toString()
            displayCount.toVisible()
            displayCount.text = item.count.toString()

        }
        dialog.decrease.setOnClickListener {
            if (item.count!! > 0) {
                item.count -= 1
                if (item.count == 0) {
                    dialog.txtCount.text = "تعداد"
                    displayCount.toGone()

                }
                displayCount.text = item.count.toString()
                dialog.txtCount.text = item.count.toString()
            }


        }

        dialog?.show()
    }


//    private fun initDialogPermission() {
//        dialogPermission = Dialog(context)
//        dialogPermission?.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialogPermission?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialogPermission?.setCancelable(false)
//        val view = LayoutInflater.from(context).inflate(R.layout.dialog_permission, null)
//        view.iv_cancel_permission?.setOnClickListener {
//            hidePermission()
//        }
//        view.tv_permission_setting?.setOnClickListener {
//            hidePermission()
//            openAppSettings()
//        }
//        dialogPermission?.setContentView(view)
//    }


    fun hideLoading() {
        Log.d("dialog uuu", "hideLoading")

        dialogLoading?.dismiss()

    }


    fun openAppSettings() {

        val packageUri = Uri.fromParts("package", context.packageName, null)

        val applicationDetailsSettingsIntent = Intent()

        applicationDetailsSettingsIntent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        applicationDetailsSettingsIntent.data = packageUri
        applicationDetailsSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        context.startActivity(applicationDetailsSettingsIntent)

    }

}



