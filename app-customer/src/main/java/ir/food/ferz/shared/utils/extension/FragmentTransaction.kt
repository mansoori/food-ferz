package ir.food.ferz.shared.utils.extension

import androidx.fragment.app.FragmentTransaction

fun FragmentTransaction.isAddToBackStack(addToStack: Boolean): FragmentTransaction {
    if (addToStack)
        addToBackStack(null)
    return this
}