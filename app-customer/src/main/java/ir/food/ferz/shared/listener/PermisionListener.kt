package ir.food.ferz.shared.listener

interface PermisionListener {
    fun onGranted()
    fun onNeverAsk()
}