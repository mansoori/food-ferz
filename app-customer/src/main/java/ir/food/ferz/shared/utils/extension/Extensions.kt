package ir.food.ferz.shared.utils.extension

import ir.food.ferz.App
import android.view.View
import android.widget.Toast




fun View.toGone() {
    visibility = View.GONE
}
fun View.toVisible() {
    visibility = View.VISIBLE
}
fun View.toInVisible() {
    visibility = View.INVISIBLE
}


fun toast(message: String?){

    Toast.makeText(App.context,message,Toast.LENGTH_SHORT).show()
}

fun toastLong(message: String?){

    Toast.makeText(App.context,message,Toast.LENGTH_LONG).show()
}
fun View.isVisible(): Boolean {
    return visibility == View.VISIBLE
}
