package ir.food.ferz.shared.utils

import timber.log.Timber

class MapUtils {
    companion object {
        fun getMapScreenShot(latitude: Double, longitude: Double, width: Int, height: Int): String {
            var markerInfo = ""
            var center = ""
            val markerIcon = "http://uupload.ir/files/ndp6_placeholder.png"

            center += "center=$latitude,$longitude&"
            markerInfo += "markers=icon:$markerIcon|shadow:true|$latitude,$longitude&"

            var screenUrl= "https://maps.googleapis.com/maps/api/staticmap?" +
                    "language=fa&" +
                    "scale=1&" +
                    center +
                    "zoom=16&" + "key=AIzaSyDA70FPTJ7cgiigWLYEsvhrJ_nNtzm34CE&" +
                    "size=" + width + "x" + height + "&" +
                    "maptype=roadmap&" +
                    "format=png&" +
                    "visual_refresh=true&" +
                    markerInfo +
                    "path=color:0x000000|weight:5"
            Timber.d("Screen=====>"+screenUrl)
            return screenUrl
        }
    }

}
//AIzaSyAEF2FnFMcEK0EAxLiPAXM9FL24aQkE_ek