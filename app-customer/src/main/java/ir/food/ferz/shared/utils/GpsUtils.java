package ir.food.ferz.shared.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.core.content.ContextCompat;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.*;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import ir.food.ferz.shared.listener.PermisionListener;
import ir.food.ferz.R;

import java.util.List;
import java.util.Objects;

/**
 * Created by root on 2/19/18.
 */

public class GpsUtils {

    private Activity activity;
    private GoogleApiClient googleApiClient;
    private static final int REQUEST_LOCATION = 199;
    private LocationManager locationManager;
    private static GpsUtils instance = null;

//    public GpsUtils(Context context) {
//        this.context = context;
//        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//    }

    public void init(Activity context) {
        this.activity = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public static GpsUtils getInstance() {
        if (instance == null) {
            instance = new GpsUtils();
        }
        return instance;
    }

    public Activity getActivity() {
        return activity;
    }


    public void enableGps() {
        if (isDisabledGps()) {
//            showEnableGpsDialogPowerSave();
            showEnableGpsDialogHigh();
        }
    }

    public boolean isDisabledGps(Context context) {
        if (context == null) return true;
        return !isHighAccuracy(context);
//        return (isDisabledGpsByMode(context) || isDisabledGpsByProvider(context));
    }

    public boolean isDisabledGps() {
        if (activity == null) return true;
        return !isHighAccuracy(activity);
//        return (isDisabledGpsByMode(activity) || isDisabledGpsByProvider(activity));
    }

    private boolean isDisabledGpsByMode(Context context) {
        return getLocationMode(context) == Settings.Secure.LOCATION_MODE_OFF;
    }

    private boolean isDisabledGpsByProvider() {
        try {
            locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                return !(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
            } else {
                return true;
            }
        } catch (Exception ex) {
            return true;
        }
    }
    private boolean isDisabledGpsByProvider(Context context) {
        try {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager != null) {
                return !(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
            } else {
                return true;
            }
        } catch (Exception ex) {
            return true;
        }
    }

    public boolean isHighAccuracy() {
        if (activity == null) return false;
        boolean isHighAccuracy;
        if ((!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || getLocationMode(activity) != Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) && isHardwareGPSDevice()) {
            isHighAccuracy = false;
        } else
            isHighAccuracy = getLocationMode(activity) != Settings.Secure.LOCATION_MODE_OFF || isHardwareGPSDevice();
        return isHighAccuracy;
    }

    public boolean isHighAccuracy(Context context) {
        if (activity == null) return false;
        boolean isHighAccuracy;
        if ((!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || getLocationMode(context) != Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) && isHardwareGPSDevice()) {
            isHighAccuracy = false;
        } else
            isHighAccuracy = getLocationMode(context) != Settings.Secure.LOCATION_MODE_OFF || isHardwareGPSDevice();
        return isHighAccuracy;
    }

    @SuppressLint("MissingPermission")
    public Location getLastKnowLocation() {
        String Provider = locationManager.getBestProvider(new Criteria(), true);
        return locationManager.getLastKnownLocation(Provider);
    }


    public boolean isHardwareGPSDevice() {
        if (locationManager == null)
            return false;
        final List<String> providers = locationManager.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private boolean isNetworkLocationEnable(LocationManager locationManager) {
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public void showEnableGpsDialogPowerSave() {
        if (activity == null)
            return;

        googleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        googleApiClient.connect();
                    }
                })
                .addOnConnectionFailedListener(connectionResult -> {
                }).build();
        googleApiClient.connect();

//            LocationRe
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(10000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in oncontextResult().
                        status.startResolutionForResult(activity, REQUEST_LOCATION);
                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                    break;
            }
        });
    }

    public void showEnableGpsDialogHigh() {
        if (activity == null)
            return;
        googleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {

                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        googleApiClient.connect();
                    }
                })
                .addOnConnectionFailedListener(connectionResult -> {
                }).build();
        googleApiClient.connect();

//            LocationRe
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in oncontextResult().
                        status.startResolutionForResult(activity, REQUEST_LOCATION);
                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                    break;
            }
        });
    }


    public void showDialogGps(final Context context) {
        if (context == null) return;
        final Dialog gpsDialog = new Dialog(context);
        gpsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        gpsDialog.setCancelable(false);
        Objects.requireNonNull(gpsDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        gpsDialog.setContentView(R.layout.dialog_gps_alert);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(gpsDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;


        View btnSetting = gpsDialog.findViewById(R.id.dialog_gps_tv_setting);
        View btnCancel = gpsDialog.findViewById(R.id.dialog_gps_tv_cancel);


        btnCancel.setOnClickListener(v -> gpsDialog.dismiss());

        btnSetting.setOnClickListener(v -> {
            getPermission(true, new PermisionListener() {
                @Override
                public void onGranted() {
                    if (!isHighAccuracy(context)){
                        showEnableGpsDialogHigh();
                    }
                }

                @Override
                public void onNeverAsk() {

                }
            });
            gpsDialog.dismiss();
        });

        gpsDialog.show();
        gpsDialog.getWindow().setAttributes(lp);

        final Handler handleGps = new Handler();
        handleGps.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (gpsDialog.isShowing() && isDisabledGps()) {
                    handleGps.postDelayed(this, 1000);
                } else {
                    handleGps.removeCallbacks(this);
                    gpsDialog.dismiss();
                }

            }
        }, 1000);
    }


    public void startSettingGps() {
        if (activity == null)
            return;
        final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        activity.startActivity(intent);
    }

    public void getPermission(boolean isNeedPermission, PermisionListener permisionListener) {
        if (activity == null ||
                (!isHasGpsPermission() && !isNeedPermission)) {
            return;
        }
        Log.d("","ACTIVE_LOCATION_ENGINE 3");
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        Log.d("","ACTIVE_LOCATION_ENGINE send");
                        if (permisionListener != null)
                            permisionListener.onGranted();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied() && isNeedPermission)
                            if (permisionListener != null)
                                permisionListener.onNeverAsk();

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public boolean isHasGpsPermission() {
        return activity != null && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private static int getLocationMode(Context context) {
        if (context == null) return Settings.Secure.LOCATION_MODE_OFF;
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }


        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (TextUtils.isEmpty(locationProviders)) {
                locationMode = Settings.Secure.LOCATION_MODE_OFF;
            } else if (locationProviders.contains(LocationManager.GPS_PROVIDER) && locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
            } else if (locationProviders.contains(LocationManager.GPS_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_SENSORS_ONLY;
            } else if (locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_BATTERY_SAVING;
            }

        }

        return locationMode;
    }

}
