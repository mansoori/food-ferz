package ir.food.ferz.shared.utils

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import ir.food.ferz.App
import ir.food.ferz.R
import ir.food.ferz.shared.listener.OnAnimationCallback
import ir.food.ferz.shared.utils.extension.isVisible
import ir.food.ferz.shared.utils.extension.toInVisible
import ir.food.ferz.shared.utils.extension.toVisible
import android.content.Context
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import com.mapbox.mapboxsdk.Mapbox
import kotlinx.android.synthetic.main.fragment_splash.*
import timber.log.Timber


/**
 * Created by alishiralizade on 7/21/17.
 */

class AnimationUtils {
    companion object {
        lateinit var animRotateAclk: Animation
        lateinit var viewRotate: View
        fun fadeIn(view: View, context: Context, callback: OnAnimationCallback?) {

            if (view.visibility == View.VISIBLE) {
                return
            }
            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.fade_in)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()
                    view.visibility = View.VISIBLE

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }

        fun fadeIn(view: View) {

            view.visibility = View.VISIBLE
            val fadeIn = AnimationUtils.loadAnimation(
                App.context,
                R.anim.fade_in
            )

            view.startAnimation(fadeIn)
        }

        fun fadeOut(view: View, context: Context, callback: OnAnimationCallback?) {

            //        if (view.getVisibility() == View.VISIBLE) {
            //            return;
            //        }
            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.fade_out)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()

                    view.visibility = View.GONE

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }

        @JvmOverloads
        fun slideUp(view: View, context: Context, callback: OnAnimationCallback? = null) {

            //        if (view.getVisibility() == View.VISIBLE) {
            //            return;
            //        }
            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.slide_in_bottom)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    view.visibility = View.VISIBLE

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }


        fun slideDown(view: View, context: Context, callback: OnAnimationCallback?) {
            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.slide_out_down)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    callback?.onStartAnimate()
                }

                override fun onAnimationEnd(animation: Animation) {
                    view.visibility = View.GONE
                    callback?.onFinishAnimate()
                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }


        fun slideInRight(view: View, context: Context?, callback: OnAnimationCallback?) {
            if (context == null)
                return
            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    callback?.onStartAnimate()

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }

        fun slideInLeft(view: View, context: Context, callback: OnAnimationCallback?) {
            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.slide_in_left)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    callback?.onStartAnimate()

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }


        fun slideOutRight(view: View, context: Context, callback: OnAnimationCallback?) {
            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.slide_out_right)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    callback?.onStartAnimate()

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }

        fun slideOutLeft(view: View, context: Context, callback: OnAnimationCallback?) {

            val animSlideDown = android.view.animation.AnimationUtils.loadAnimation(context, R.anim.slide_out_left)
            animSlideDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    callback?.onStartAnimate()

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(animSlideDown)
        }

        fun animateSlideOutBottom(view: View) {
            val slideUpAnimation = AnimationUtils.loadAnimation(
                App.context,
                R.anim.slide_out_bottom
            )
            slideUpAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                }

                override fun onAnimationEnd(animation: Animation) {
                    view.visibility = View.INVISIBLE
                }

                override fun onAnimationRepeat(animation: Animation) {
                }
            })
            view.startAnimation(slideUpAnimation)

        }

        fun animateSlideInBottom(view: View) {
            view.visibility = View.VISIBLE
            val slideDownAnimation = AnimationUtils.loadAnimation(
                App.context,
                R.anim.slide_in_bottom
            )
            view.startAnimation(slideDownAnimation)
        }

        fun animateSlideInBottom(view: View, callback: OnAnimationCallback?) {
            view.visibility = View.VISIBLE
            val slideDownAnimation = AnimationUtils.loadAnimation(
                App.context,
                R.anim.slide_in_bottom
            )
            slideDownAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    callback?.onStartAnimate()

                }

                override fun onAnimationEnd(animation: Animation) {
                    callback?.onFinishAnimate()

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(slideDownAnimation)
        }

        fun animateSlideOutTop(view: View) {
            val slideUpAnimation = AnimationUtils.loadAnimation(App.context, R.anim.slide_out_top)
            slideUpAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {

                }

                override fun onAnimationEnd(animation: Animation) {
                    view.toInVisible()

                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
            view.startAnimation(slideUpAnimation)

        }

        fun animateSlideInTop(view: View) {
            val slideDownAnimation = AnimationUtils.loadAnimation(
                App.context,
                R.anim.slide_in_top
            )
            view.startAnimation(slideDownAnimation)
            view.toVisible()

        }

        fun animateSlideFadeOutBottom(view: View, checkVisibility: Boolean = false) {
            if (checkVisibility && !view.isVisible())
                return
            view.toInVisible()
            val slideUpAnimation = AnimationUtils.loadAnimation(App.context, R.anim.slide_fade_out_bottom)
            view.startAnimation(slideUpAnimation)

        }

        fun animateSlideFadeInBottom(view: View, checkVisibility: Boolean = false) {
            if (checkVisibility && view.isVisible())
                return
            view.toVisible()
            val slideDownAnimation = AnimationUtils.loadAnimation(App.context, R.anim.slide_fade_in_bottom)
//             slideDownAnimation.fillAfter = true
//             slideDownAnimation.isFillEnabled = true
            view.startAnimation(slideDownAnimation)
        }

        fun animateSlideFadeOutTop(view: View, checkVisibility: Boolean = false) {
            if (checkVisibility && !view.isVisible())
                return
            view.toInVisible()
            val set = AnimationSet(true)
//            val animateSize = (view.height * 3) / 2
//            val animation = TranslateAnimation(0f, 0f, 0f, -animateSize.toFloat())
//            set.addAnimation(animation)
            val fadeAnimate = AlphaAnimation(1.0f, 0.0f)
            set.addAnimation(fadeAnimate)
            set.duration = 200
//            set.duration = 500
            view.startAnimation(set)
        }

        fun animateSlideFadeInTop(view: View, checkVisibility: Boolean = false) {
            if (checkVisibility && view.isVisible())
                return
            view.toVisible()
            val set = AnimationSet(true)
//            val animateSize = (view.height * 3) / 2
//            val animation = TranslateAnimation(0f, 0f, -animateSize.toFloat(),0f)
//            set.addAnimation(animation)
            val fadeAnimate = AlphaAnimation(0.0f, 1.0f)
            set.addAnimation(fadeAnimate)
            set.duration = 300
//            set.duration = 500
            view.startAnimation(set)
        }

        fun animateSlideTop(view: View) {
//            val slideUpAnimation = AnimationUtils.loadAnimation(App.context, R.anim.slide_top)
//            slideUpAnimation.fillAfter = true
//            slideUpAnimation.isFillEnabled = true
//            view.startAnimation(slideUpAnimation)


//            val animateSize = (view.height * 3) / 2
//            val valueAnimator = ValueAnimator.ofFloat(view.y,view.y-animateSize)
//            valueAnimator.duration = 400
//            valueAnimator.addUpdateListener {
//                view.y = it?.animatedValue as Float
//            }
//            valueAnimator.start()


            val animateSize = (view.height * 3) / 2
            val animation = ObjectAnimator.ofFloat(view, "translationY", -animateSize.toFloat())
            animation.duration = 400
            animation.start()

        }

        fun animateSlideBottom(view: View) {
//            val slideUpAnimation = AnimationUtils.loadAnimation(App.context, R.anim.slide_bottom)
//            view.startAnimation(slideUpAnimation)


//            val animateSize = (view.height * 3) / 2
//            val valueAnimator = ValueAnimator.ofFloat(view.y,view.y+animateSize)
//            valueAnimator.duration = 400
//            valueAnimator.addUpdateListener {
//                view.y = it?.animatedValue as Float
//            }
//            valueAnimator.start()


//            val animateSize = (view.height * 3) / 2
            val animation = ObjectAnimator.ofFloat(view, "translationY", 0f)
            animation.duration = 400
            animation.start()

        }

        fun animateSlide(view: View, animateSize: Float) {
            Timber.d("view.y => ${view.y} animateSize => $animateSize")
//            val animateSize = (view.height * 3) / 2
            val valueAnimator = ValueAnimator.ofFloat(view.y, animateSize)
            valueAnimator.duration = 200
            valueAnimator.addUpdateListener {
                view.y = it?.animatedValue as Float
            }
            valueAnimator.start()

        }

        fun rotationAnimate(context: Context, view: View) {
            viewRotate = view
            animRotateAclk = AnimationUtils.loadAnimation(context, R.anim.linear_interpolator)
            view.startAnimation(animRotateAclk)

        }

        fun stopRouteAnimation() {
            viewRotate.clearAnimation()
        }

        fun onShakeAnimation(view: View, context: Context) {
            val shake: Animation = AnimationUtils.loadAnimation(context, R.anim.shake)

            view.startAnimation(shake) // starts animation
        }
    }


}
