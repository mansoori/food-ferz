package ir.food.ferz.shared.utils




const val PREF_KEY_IS_LOGIN = "PREF_KEY_IS_LOGIN"
const val PREF_KEY_USER_ID = "PREF_KEY_USER_ID"
const val PREF_KEY_IS_ADDRESS_SET = "PREF_KEY_IS_ADDRESS_SET"
const val PREF_NAME = "foodFerzKeyPref"
const val DEFAULT_TYPEFACE = "fonts/IRANSans-Light.ttf"
const val API_URL = "http://185.208.174.18:8080/foodfrez/restful/services/"
const val IMAGE_URL = API_URL+"getImage?imageName="

const val DRAWABLE_LEFT:Int=0
const val DRAWABLE_RIGHT:Int=1
const val DRAWABLE_TOP:Int=2
const val DRAWABLE_BOTTOM:Int=3
