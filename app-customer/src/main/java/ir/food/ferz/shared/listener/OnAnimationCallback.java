package ir.food.ferz.shared.listener;

/**
 * Created by p.khalaj on 7/24/2017.
 */

public interface OnAnimationCallback {
    void onStartAnimate();
    void onFinishAnimate();
}
