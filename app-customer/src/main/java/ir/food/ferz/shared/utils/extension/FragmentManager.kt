package ir.food.ferz.shared.utils.extension

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

internal fun FragmentManager.removeFragment(tag: String?) {

    this.findFragmentByTag(tag)?.let {
        this.beginTransaction()
            .disallowAddToBackStack()
            .remove(it)
            .commitNow()
    }


}

internal fun FragmentManager.addFragment(containerViewId: Int,
                                         fragment: Fragment,
                                         tag: String) {
    this.beginTransaction()
            .add(containerViewId, fragment, tag)
            .addToBackStack(tag)
            .commit()

}

internal fun FragmentManager.ReplaceFragment(containerViewId: Int,
                                             fragment: Fragment,
                                             tag: String) {

    if (this.findFragmentByTag(fragment.tag) == null) {
        this.beginTransaction()
                .addToBackStack(tag)
                .replace(containerViewId, fragment, tag)
                .addToBackStack(tag)
                .commit()
    }
}

