package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.data.di.builder.MainFragmentBuilder
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.features.activity.main.MainViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [MainActivityModule.ProvideViewModel::class, MainFragmentBuilder::class])
class MainActivityModule {


    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: MainActivity
        ) = ViewModelProviders.of(target, factory).get(MainViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(MainViewModel::class)
        fun provideViewModel(): ViewModel =

            MainViewModel()

    }
}