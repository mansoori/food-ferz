package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.*
import ir.food.ferz.features.splash.SplashFragment
import ir.food.ferz.features.splash.SplashViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [BookmarkFragmentModule.ProvideViewModel::class])
class BookmarkFragmentModule {


    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: BookmarkFragment
        ) = ViewModelProviders.of(target, factory).get(BookmarkViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(BookmarkViewModel::class)
        fun provideViewModel(): ViewModel =
            BookmarkViewModel()

    }
}