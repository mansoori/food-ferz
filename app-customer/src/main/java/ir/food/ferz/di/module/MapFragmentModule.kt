package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.AddressViewModel
import ir.food.ferz.features.address.map.MapFragment
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [MapFragmentModule.ProvideViewModel::class])
class MapFragmentModule {


//    @Provides
//    fun provideLocationEngine(context: Context) = LocationEngineProvider.getBestLocationEngine(context)
//
//    @Provides
//    fun provideLocationEngineRequest() = LocationEngineRequest.Builder(AppConfig.UPDATE_INTERVAL_IN_MILLISECONDS)
//        .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
//        .setFastestInterval(AppConfig.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
//        .build()!!

    @Module
    class InjectViewModel {



        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: MapFragment
        ) = ViewModelProviders.of(target, factory).get(AddressViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(AddressViewModel::class)
        fun provideViewModel(): ViewModel =
            AddressViewModel()

    }
}