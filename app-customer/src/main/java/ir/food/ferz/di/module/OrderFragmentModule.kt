package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.*
import ir.food.ferz.features.splash.SplashFragment
import ir.food.ferz.features.splash.SplashViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [OrderFragmentModule.ProvideViewModel::class])
class OrderFragmentModule {


    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: OrderFragment
        ) = ViewModelProviders.of(target, factory).get(OrderViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(OrderViewModel::class)
        fun provideViewModel(): ViewModel =
            OrderViewModel()

    }
}