package ir.food.ferz.data.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ir.food.ferz.App
import ir.food.ferz.data.di.builder.MainActivityBuilder
import ir.food.ferz.data.di.module.AppModule
import ir.food.ferz.features.base.BaseViewModel
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        (AndroidSupportInjectionModule::class),
        (AppModule::class),
        (MainActivityBuilder::class)]
)

interface AppComponent : AndroidInjector<App> {

//    @Component.Builder
//    abstract class Builder : AndroidInjector.Builder<App>()


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(instance: Application)
    //
    fun inject(baseViewModel: BaseViewModel)

}