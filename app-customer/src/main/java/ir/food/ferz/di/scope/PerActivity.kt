package ir.food.ferz.data.di.scope

import javax.inject.Scope

@Scope  // create scope for create only one instance object
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity {
}