package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.FoodsFragment
import ir.food.ferz.features.home.FoodsViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [FoodFragmentModule.ProvideViewModel::class])
class FoodFragmentModule {


    @Module
    class InjectViewModel {


        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: FoodsFragment
        ) = ViewModelProviders.of(target, factory).get(FoodsViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(FoodsViewModel::class)
        fun provideViewModel(): ViewModel =
            FoodsViewModel()

    }
}