package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.PreviewHomeFragment
import ir.food.ferz.features.home.PreviewHomeViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [PreviewHomeFragmentModule.ProvideViewModel::class])
class PreviewHomeFragmentModule {


    @Module
    class InjectViewModel {



        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: PreviewHomeFragment
        ) = ViewModelProviders.of(target, factory).get(PreviewHomeViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(PreviewHomeViewModel::class)
        fun provideViewModel(): ViewModel =
            PreviewHomeViewModel()

    }
}