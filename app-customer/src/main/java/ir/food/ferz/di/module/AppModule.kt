package ir.food.ferz.data.di.module

//import ir.namaa.api.pay.NamaaPayApiEndPoint
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import ir.food.ferz.data.di.BaseViewModelFactory
import ir.food.ferz.data.prefs.AppPreferencesHelper
import ir.food.ferz.data.prefs.PreferencesHelper
import ir.food.ferz.data.repository.ApiService
import ir.food.ferz.data.repository.AppRepository
import ir.food.ferz.data.repository.DataField
import ir.food.ferz.data.room.LocalCacheManager
import ir.food.ferz.data.utils.database.AppDatabase
import ir.food.ferz.shared.utils.DialogUtils
import javax.inject.Provider
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    internal fun provideCompositeDisposable() = CompositeDisposable()

    @Provides
    @Singleton
    internal fun provideDataField() = DataField()

    @Provides
    @Singleton
    internal fun provideStatusDialog() = DialogUtils.getInstance()

//    @Provides
//    @Singleton
//    fun provideDatabase(context: Context) = AppDatabase.buildDatabase(context).foodDao()
//
//    @Provides
//    @Singleton
//    fun provideLocalCachManager(context: Context) =LocalCacheManager.getInstance(context)

//    @Provides
//    internal fun provideGpsUtils() = GpsUtils.getInstance()


    @Provides
    fun provideApiService() = ApiService()

    @Provides
    @Singleton
    fun provideAppRepository(
        apiService: ApiService,
        preferencesHelper: PreferencesHelper,
        context: Context,
        dataField: DataField
    ) = AppRepository(apiService, preferencesHelper, context, dataField)

//    @Provides
//    fun provideApiEndPoint() = NamaaPayApiEndPoint(BuildConfig.BASE_URL, BuildConfig.BASE_URL_LOGIN,BuildConfig.BASE_URL_ADMIN,BuildConfig.BASE_URL_SUGGEST)

    @Provides
    @Singleton
    fun providePreference(context: Context) =
        PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun providePreferenceHelper(preferences: SharedPreferences): PreferencesHelper =
        AppPreferencesHelper(preferences)


    @Provides
    fun provideViewModelFactory(
        providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ): ViewModelProvider.Factory =
        BaseViewModelFactory(providers)
}