package ir.food.ferz.data.di.builder


import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.food.ferz.features.activity.main.MainActivity
import ir.food.ferz.data.di.module.MainActivityModule
import ir.food.ferz.data.di.scope.PerActivity


@Module
abstract class MainActivityBuilder {

    //    @ContributesAndroidInjector(modules = [MainActivityModule::class
//        SplashFragmentProvider::class,
//        PlaceInfoFragmentProvider::class,
//        PoiFragmentProvider::class,
//        PoiEditFragmentProvider::class,
//        ProfileFragmentProvider::class,
//        SavedFavoritsFragmentProvider::class,
//        UserActivityLogFragmentProvider::class
//    ])


    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class, MainActivityModule.InjectViewModel::class])
    abstract fun bindMainActivity(): MainActivity
}