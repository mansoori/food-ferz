package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.VerifyFragment
import ir.food.ferz.features.login.LoginViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [VerifyCodeFragmentModule.ProvideViewModel::class])
class VerifyCodeFragmentModule {


    @Module
    class InjectViewModel {



        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: VerifyFragment
        ) = ViewModelProviders.of(target, factory).get(LoginViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(LoginViewModel::class)
        fun provideViewModel(): ViewModel =
            LoginViewModel()

    }
}