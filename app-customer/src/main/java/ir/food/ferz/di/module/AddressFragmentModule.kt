package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.*
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [AddressFragmentModule.ProvideViewModel::class])
class AddressFragmentModule {


    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: MyAddressFragment
        ) = ViewModelProviders.of(target, factory).get(AddressViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(AddressViewModel::class)
        fun provideViewModel(): ViewModel =
            AddressViewModel()

    }
}