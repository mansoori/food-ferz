package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.HomeFragment
import ir.food.ferz.features.home.HomeViewModel
import ir.food.ferz.features.login.LoginFragment
import ir.food.ferz.features.login.LoginViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [LoginFragmentModule.ProvideViewModel::class])
class LoginFragmentModule {


    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: LoginFragment
        ) = ViewModelProviders.of(target, factory).get(LoginViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(LoginViewModel::class)
        fun provideViewModel(): ViewModel =
            LoginViewModel()

    }
}