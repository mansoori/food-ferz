package ir.food.ferz.data.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ir.food.ferz.features.login.LoginFragment
import ir.food.ferz.features.splash.SplashFragment
import ir.food.ferz.data.di.module.*
import ir.food.ferz.data.di.scope.PerFragment
import ir.food.ferz.features.home.*
import ir.food.ferz.features.address.map.MapFragment


@Module
abstract class MainFragmentBuilder {

    @PerFragment
    @ContributesAndroidInjector(modules = [SplashFragmentModule::class, SplashFragmentModule.InjectViewModel::class])
    internal abstract fun provideSplashFragment(): SplashFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [LoginFragmentModule::class, LoginFragmentModule.InjectViewModel::class])
    internal abstract fun provideLoginFragment(): LoginFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [VerifyCodeFragmentModule::class, VerifyCodeFragmentModule.InjectViewModel::class])
    internal abstract fun provideVerifyFragment(): VerifyFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [MapFragmentModule::class, MapFragmentModule.InjectViewModel::class])
    internal abstract fun provideMapFragmentModule(): MapFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [PreviewHomeFragmentModule::class, PreviewHomeFragmentModule.InjectViewModel::class])
    internal abstract fun providePreviewHomeFragment(): PreviewHomeFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class, HomeFragmentModule.InjectViewModel::class])
    internal abstract fun provideHomeFragment(): HomeFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [ProfileFragmentModule::class, ProfileFragmentModule.InjectViewModel::class])
    internal abstract fun provideProfileFragment(): ProfileFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [AddCreditsFragmentModule::class, AddCreditsFragmentModule.InjectViewModel::class])
    internal abstract fun provideAddCreditFragment(): AddCreditsFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [DiscountFragmentModule::class, DiscountFragmentModule.InjectViewModel::class])
    internal abstract fun provideDisCountFragment(): DiscountFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [ReViewFragmentModule::class, ReViewFragmentModule.InjectViewModel::class])
    internal abstract fun provideReViewFragment(): ReViewFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [AddReViewFragmentModule::class, AddReViewFragmentModule.InjectViewModel::class])
    internal abstract fun provideAddReViewFragment(): AddReViewFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [BascketFragmentModule::class, BascketFragmentModule.InjectViewModel::class])
    internal abstract fun provideBascketFragment(): BascketFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [AddressFragmentModule::class, AddressFragmentModule.InjectViewModel::class])
    internal abstract fun provideAddressFragment(): MyAddressFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [AddAddressFragmentModule::class, AddAddressFragmentModule.InjectViewModel::class])
    internal abstract fun provideAddAddressFragment(): AddAddressFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [FoodFragmentModule::class, FoodFragmentModule.InjectViewModel::class])
    internal abstract fun provideFoodFragment(): FoodsFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [OrderFragmentModule::class, OrderFragmentModule.InjectViewModel::class])
    internal abstract fun provideOrderFragment(): OrderFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [BookmarkFragmentModule::class, BookmarkFragmentModule.InjectViewModel::class])
    internal abstract fun provideBookMarkFragment(): BookmarkFragment

}