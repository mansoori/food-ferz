package ir.food.ferz.data.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.food.ferz.features.home.AddReViewFragment
import ir.food.ferz.features.home.ProfileFragment
import ir.food.ferz.features.home.ProfileViewModel
import ir.food.ferz.features.home.ReViewFragment
import ir.food.ferz.features.splash.SplashFragment
import ir.food.ferz.features.splash.SplashViewModel
import lv.chi.example.vmdagger.ui.core.di.ViewModelKey


@Module(includes = [AddReViewFragmentModule.ProvideViewModel::class])
class AddReViewFragmentModule {


    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: AddReViewFragment
        ) = ViewModelProviders.of(target, factory).get(ProfileViewModel::class.java)

    }

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(ProfileViewModel::class)
        fun provideViewModel(): ViewModel =
            ProfileViewModel()

    }
}