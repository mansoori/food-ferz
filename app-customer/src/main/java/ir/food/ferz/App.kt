package ir.food.ferz

import android.content.Context
import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import ir.food.ferz.data.di.component.AppComponent
import ir.food.ferz.data.di.component.DaggerAppComponent
import timber.log.Timber


class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerAppComponent.builder().application(this).build()
        return appComponent
    }


    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        App.context = this.applicationContext


        //setup fabric
//   Fabric.with(this, Crashlytics())

    }

    companion object {
        internal var context: Context? = null
        lateinit var appComponent: AppComponent

        private var bus: Bus? = null


        fun getBus(): Bus {
            if (bus == null) {
                bus = Bus(ThreadEnforcer.MAIN)
            }
            return bus as Bus
        }


    }
}