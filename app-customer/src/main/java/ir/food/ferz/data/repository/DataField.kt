package ir.food.ferz.data.repository

import ir.food.ferz.data.model.request.FoodsJson
import ir.food.ferz.data.model.response.BaseInfoResponse
import ir.food.ferz.data.model.response.GetAllFoodResponse


class DataField {

    var baseInfo: BaseInfoResponse? = null

    var listFoodLocal: ArrayList<GetAllFoodResponse> =ArrayList()

    val listFood: ArrayList<FoodsJson> = ArrayList()

}