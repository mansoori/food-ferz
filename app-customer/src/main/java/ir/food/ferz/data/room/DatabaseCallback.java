package ir.food.ferz.data.room;

import java.util.List;

/**
 * Created by alahammad on 10/4/17.
 */

public interface DatabaseCallback {

    void onFoodsLoaded(List<FoodStorged> foodStorgeds);

    void onFoodsDeleted();

    void onFoodsAdded();

    void onDataNotAvailable();

    void onFoodUpdated();
}
