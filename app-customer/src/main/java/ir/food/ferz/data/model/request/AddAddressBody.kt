package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName

data class AddAddressBody(

    @SerializedName("userId")
    val userId: Long,


    @SerializedName("province")
    val province: String,

    @SerializedName("provinceId")
    val provinceId: Int,


    @SerializedName("city")
    val city: String,

    @SerializedName("cityId")
    val cityId: Int,


    @SerializedName("address")
    val address: String,


    @SerializedName("isSelect")
    val isSelect: Boolean,

    @SerializedName("lat")
    val lat: String,

    @SerializedName("lng")
    val lng: String


)
