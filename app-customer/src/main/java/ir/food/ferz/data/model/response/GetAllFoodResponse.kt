package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class GetAllFoodResponse(


    @SerializedName("id") val id : Long,
    @SerializedName("foodCategoryId") val foodCategoryId : Long,
    @SerializedName("foodTypeId") val foodTypeId : Long,
    @SerializedName("name") val name : String,
    @SerializedName("description") val description : String,
    @SerializedName("price") val price : Int,
    @SerializedName("imageName") val imageName : String,
    @SerializedName("isWeeklyFood") val isWeeklyFood : Int,
    @SerializedName("isActive") val isActive : Boolean,
    @SerializedName("isPizza") val isPizza : Boolean,
    @SerializedName("pizzaNum") val pizzaNum : String,
    @SerializedName("count") var count : Int
)