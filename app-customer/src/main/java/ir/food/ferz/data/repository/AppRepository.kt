package ir.food.ferz.data.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable
import io.reactivex.Single
import ir.food.ferz.App
import ir.food.ferz.data.model.request.AddAddressBody
import ir.food.ferz.data.model.request.AddBascketBody
import ir.food.ferz.data.model.request.UpdateAddressBody
import ir.food.ferz.data.model.request.UpdateUserBody
import ir.food.ferz.data.model.response.*
import ir.food.ferz.data.prefs.PreferencesHelper
import ir.food.ferz.features.base.BaseRepository
import ir.food.ferz.shared.utils.API_URL
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import javax.inject.Inject

class AppRepository @Inject constructor(
    apiService: ApiService,
    preferencesHelper: PreferencesHelper,
    context: Context,
    dataField: DataField
) : BaseRepository(apiService, preferencesHelper, context, dataField) {




    //
    fun getTestApi(
        userBusinessList: MutableLiveData<testResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.testApi().setWorker().runApi(userBusinessList, isLoadingApi, errorApi, ApiEnums.TEST)

    fun getBaseInfo(
        baseInfo: MutableLiveData<BaseInfoResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.getBaseInfo().setWorker().runApi(baseInfo, isLoadingApi, errorApi, ApiEnums.BASE_INFO)

    fun getRegister(
        mobile: String,
        register: MutableLiveData<RegisterResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.getRegister(mobile).setWorker().runApi(register, isLoadingApi, errorApi, ApiEnums.REGISTER)

    fun getVerify(
        mobile: String,
        verify: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.getVerify(mobile).setWorker().runApi(verify, isLoadingApi, errorApi, ApiEnums.VERIFY_CODE)

    fun getUserUpdate(
        userBody: UpdateUserBody,
        verify: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.getUpdateUser(userBody).setWorker().runApi(verify, isLoadingApi, errorApi, ApiEnums.USER_UPDATE)

    fun getReView(
        index: String,
        verify: MutableLiveData<List<ReViewResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.getReviews(index).setWorker().runApi(verify, isLoadingApi, errorApi, ApiEnums.GET_REVIEW)

    fun getaddCredits(
        userId: String, credit: String,
        verify: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.getaddCredits(userId, credit).setWorker().runApi(verify, isLoadingApi, errorApi, ApiEnums.ADD_CREDIT)

    fun getverfiyDiscount(
        userId: String, discountCode: String,
        verify: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) =
        apiService.getverfiyDiscount(userId, discountCode).setWorker().runApi(
            verify,
            isLoadingApi,
            errorApi,
            ApiEnums.VERIFY_DISCOUNT
        )


    fun getProvince(
        provinceResponse: MutableLiveData<List<GetProvinceResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getProvince().setWorker().runApi(provinceResponse, isLoadingApi, errorApi, ApiEnums.GET_PROVINCE)


    fun getCity(
        provinceId: String,
        cityResponse: MutableLiveData<List<GetCityResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getCity(provinceId).setWorker().runApi(cityResponse, isLoadingApi, errorApi, ApiEnums.GET_CITY)

    fun getAddAddress(
        addAddressBody: AddAddressBody,
        cityResponse: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getAddAdress(addAddressBody).setWorker().runApi(
        cityResponse,
        isLoadingApi,
        errorApi,
        ApiEnums.GET_ADD_ADDRESS
    )

    fun getAddress(
        userId: String,
        getAddress: MutableLiveData<List<GetAddressResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getAddress(userId).setWorker().runApi(
        getAddress,
        isLoadingApi,
        errorApi,
        ApiEnums.GET_ADDRESS
    )

    fun getUpdateAddress(
        updateAddressBody: UpdateAddressBody, updateAddressResult: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getUpdateAddress(updateAddressBody).setWorker().runApi(
        updateAddressResult,
        isLoadingApi,
        errorApi,
        ApiEnums.UPDATE_ADDRESS
    )


    fun getDeleteAddress(
        addressId: String,
        deleteAddress: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getDeleteAddress(addressId).setWorker().runApi(
        deleteAddress,
        isLoadingApi,
        errorApi,
        ApiEnums.DELETE_ADDRESS
    )


    fun getAllFood(
        foodCategoryId: String, foodTypeId: String, foodId: String, isWeeklyFood: String, index: String,
        allFood: MutableLiveData<List<GetAllFoodResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getAllFoods(foodCategoryId, foodTypeId, foodId, isWeeklyFood, index).setWorker().runApi(
        allFood,
        isLoadingApi,
        errorApi,
        ApiEnums.GET_ALL_FOOD
    )

    fun getAddBascket(
        addBascketBody: AddBascketBody,
        addBasket: MutableLiveData<VerifyResponse>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getAddBascket(addBascketBody).setWorker().runApi(
        addBasket, isLoadingApi, errorApi, ApiEnums.GET_ADD_BASCKET
    )


    fun getBascket(
        userId: String,
        getBascket: MutableLiveData<List<GetBascketResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getBascket(userId).setWorker().runApi(getBascket, isLoadingApi, errorApi, ApiEnums.GET_BASCKET)


    fun getFoodType(
        foodCategoryId: String,
        getBascket: MutableLiveData<List<GetFoodTypeResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getFoodType(foodCategoryId).setWorker().runApi(
        getBascket,
        isLoadingApi,
        errorApi,
        ApiEnums.GET_FOOD_TYPE
    )
//todo fix
    fun getOrder(
        foodCategoryId: String,
        getBascket: MutableLiveData<List<GetFoodTypeResponse>>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>
    ) = apiService.getFoodType(foodCategoryId).setWorker().runApi(
        getBascket,
        isLoadingApi,
        errorApi,
        ApiEnums.GET_FOOD_TYPE
    )

}
