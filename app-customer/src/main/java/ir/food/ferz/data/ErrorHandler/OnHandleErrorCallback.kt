package ir.food.ferz.data.ErrorHandler

import android.content.Context
import android.widget.Toast
import ir.food.ferz.R


abstract class OnHandleErrorCallback(var activity: Context) {
    open fun onConnectionLost(){
        Toast.makeText(activity, activity?.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

    }
    open fun onInternetLost(){
        Toast.makeText(activity, activity?.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

    }

    open fun onCaptchaNeeded(message: String?){
//        Toast.makeText(activity, activity.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

    }

    open fun onUnknownError(message: String?) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }

    open fun onUserNotExist(message: String?) {}
    open fun onIncurrentData(message: String?) {}
}
