package ir.food.ferz.data.repository;

import org.jetbrains.annotations.NotNull;

public enum ApiEnums {
    BASE_INFO,
    REGISTER,
    VERIFY_CODE,
    TEST,
    GET_REVIEW,
    ADD_CREDIT,
    VERIFY_DISCOUNT,
    USER_UPDATE,
    GET_PROVINCE,
    GET_CITY,
    GET_ADDRESS,
    GET_ADD_ADDRESS,
    UPDATE_ADDRESS,
    DELETE_ADDRESS,
    GET_ALL_FOOD,
    GET_ADD_BASCKET,
    GET_BASCKET,
    GET_FOOD_TYPE
}
