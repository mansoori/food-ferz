package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName
data class GetBascketResponse (

	@SerializedName("id") val id : Long,
	@SerializedName("user") val user : User,
	@SerializedName("foods") val foods : List<GetAllFoodResponse>,
	@SerializedName("payStatusId") val payStatusId : Long,
	@SerializedName("payStatusName") val payStatusName : String
)