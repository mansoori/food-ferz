package ir.food.ferz.data.room;

import androidx.room.*;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

import java.util.List;

/**
 * Created by alahammad on 10/2/17.
 */

@Dao
public interface FoodDao {

    @Query("SELECT * FROM foods")
    Maybe<List<FoodStorged>> getAll();

    @Query("SELECT * FROM  foods WHERE id IN (:id)")
    Flowable<List<FoodStorged>> loadAllByIds(int[] id);

    @Query("SELECT * FROM  foods WHERE name LIKE :first AND "
            + "imageName LIKE :last LIMIT 1")
    FoodStorged findByName(String first, String last);

    @Query("SELECT * FROM  foods where id = :id")
    Maybe<FoodStorged> findById(int id);

    @Insert
    void insertAll(FoodStorged... foods);

    @Delete
    void delete(FoodStorged user);

    @Update
    void updateFood(FoodStorged... foods);

}
