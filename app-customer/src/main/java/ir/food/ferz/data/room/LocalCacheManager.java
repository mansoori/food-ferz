package ir.food.ferz.data.room;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.room.Room;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ir.food.ferz.data.utils.database.AppDatabase;

import java.util.List;

/**
 * Created by alahammad on 10/4/17.
 */


public class LocalCacheManager {
    private static final String DB_NAME = "foodFerz.db";
    private Context context;
    private static LocalCacheManager _instance;
    private AppDatabase db;

    public static LocalCacheManager getInstance(Context context) {
        if (_instance == null) {
            _instance = new LocalCacheManager(context);
        }
        return _instance;
    }

    public LocalCacheManager(Context context) {
        this.context = context;
        db = Room.databaseBuilder(context, AppDatabase.class, DB_NAME).build();
    }

    @SuppressLint("CheckResult")
    public void getFoods(final DatabaseCallback databaseCallback) {
        db.foodDao().getAll().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List< FoodStorged>>() {
            @Override
            public void accept(@io.reactivex.annotations.NonNull List< FoodStorged>  foods) throws Exception {
                databaseCallback.onFoodsLoaded( foods);

            }
        });
    }

    public void addFood(FoodStorged foodStorged,final DatabaseCallback databaseCallback) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
//                 FoodStorged foodStorged = new  FoodStorged(firstName, lastName);
                db.foodDao().insertAll(foodStorged);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
                databaseCallback.onFoodsAdded();
            }

            @Override
            public void onError(Throwable e) {
                databaseCallback.onDataNotAvailable();
            }
        });
    }

    public void deleteFood(final DatabaseCallback databaseCallback, final  FoodStorged user) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.foodDao().delete(user);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                databaseCallback.onFoodsDeleted();
            }

            @Override
            public void onError(Throwable e) {
                databaseCallback.onDataNotAvailable();
            }
        });
    }


    public void updateFood(final DatabaseCallback databaseCallback, final  FoodStorged food) {
//        food.setCount(count);
//        user.setLastName("last name last name");
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                db.foodDao().updateFood(food);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {
                databaseCallback.onFoodUpdated();
            }

            @Override
            public void onError(Throwable e) {
                databaseCallback.onDataNotAvailable();
            }
        });
    }
}
