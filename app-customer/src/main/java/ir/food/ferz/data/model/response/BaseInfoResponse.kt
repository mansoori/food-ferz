package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class BaseInfoResponse (

    @SerializedName("foodsCategorys") val foodsCategorys : List<FoodsCategorys>,
    @SerializedName("foodsTypes") val foodsTypes : List<FoodsTypes>,
    @SerializedName("pizzaItems") val pizzaItems : List<String>
)