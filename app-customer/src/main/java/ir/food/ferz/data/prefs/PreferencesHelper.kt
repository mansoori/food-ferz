package ir.food.ferz.data.prefs

interface PreferencesHelper {

    var isLogin: Boolean
    var isAddressSet: Boolean
    var userId: Long
//    var userProfile: LoginResponse?

}
