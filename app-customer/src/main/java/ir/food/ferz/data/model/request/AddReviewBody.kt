package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName
import ir.food.ferz.data.model.response.Reviews

data class AddReviewBody(

    @SerializedName("userId")
    val userId: Long,


    @SerializedName("json")
    val review: Reviews


)
