package ir.food.ferz.data.ErrorHandler

import ir.food.ferz.App
import ir.food.ferz.R
import android.widget.Toast
import com.androidnetworking.error.ANError
import java.io.IOException
import java.net.ConnectException
import java.net.UnknownHostException


class
HandleError(e: Throwable, listener: OnHandleErrorCallback) {

    init {

        if (e is ConnectException || e is UnknownHostException || e is IOException
            || (e.message != null && e.message!!.contains("Unable to resolve host"))
        ) run {

            Toast.makeText(App.context, App.context?.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

        } else {

            if (((e as ANError).errorCode == 502) || (e.errorCode == 500) || (e.errorCode == 3011)) {


                Toast.makeText(App.context, App.context?.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
            } else if (e.errorCode == 5001) {

                Toast.makeText(App.context, App.context?.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

            } else if (e.errorCode == 0 && e.errorDetail == "connectionError") {

                Toast.makeText(App.context, App.context?.getString(R.string.server_error), Toast.LENGTH_SHORT).show()

            } else {
                //todo fix after error class handle

/*
                try {
                    val errString = e .errorBody
                    val response: BaseLoginResponse =
                        Gson().fromJson<Any>(errString, BaseLoginResponse::class.java) as BaseLoginResponse
                    when (response.errorCode) {
                        4025 -> listener.onIncurrentData(response.message)

                        else -> {
                            Toast.makeText(App.context,response.message,Toast.LENGTH_SHORT).show()
                        }
                    }


                } catch (e: Exception) {

                    Toast.makeText(App.context,App.context?.getString(R.string.server_error),Toast.LENGTH_SHORT).show()

                }

*/

            }
        }
    }
}
