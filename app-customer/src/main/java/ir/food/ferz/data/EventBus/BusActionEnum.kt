package ir.food.ferz.data.EventBus

enum class BusActionEnum {
    CLOSE_PRESSED,
    BACK_PRESSED,
    HIDE_KEYBOARD,
    SHOW_KEYBOARD,
    HIDE_BOTTOM_SHEET,
    GET_CITY,
    GET_ALL_FOOD,
    MY_ADDRESS,
    MY_BASCKET,
    ADD_BASCKET,
    UPDATE_ADDRESS,
    PROFILE,
    ADD_ADDRESS,
    BOOKMARK,
    ORDERS,
    USER_EXIT
}