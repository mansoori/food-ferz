package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName

data class SignUpBody(

    @SerializedName("mobile")
    val mobile: String,


    @SerializedName("name")
    val name: String,

    @SerializedName("mail")
    val mail: String,

    @SerializedName("refer")
    val refer: String,

    @SerializedName("step")
    val step: Int,


    @SerializedName("pass")
    val pass: String,

    @SerializedName("sms")
    val sms: String

)