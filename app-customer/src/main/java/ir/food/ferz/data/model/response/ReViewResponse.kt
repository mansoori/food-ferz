package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName



data class ReViewResponse (

	@SerializedName("user") val user : User,
	@SerializedName("reviews") val reviews : Reviews,
	@SerializedName("reviewFoods") val reviewFoods : List<Any>
)