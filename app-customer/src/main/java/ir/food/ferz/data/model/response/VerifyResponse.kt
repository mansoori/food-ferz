package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class VerifyResponse (

    @SerializedName("code") val code :Int,
    @SerializedName("des") val des : String,
    @SerializedName("id") val id : Long
)