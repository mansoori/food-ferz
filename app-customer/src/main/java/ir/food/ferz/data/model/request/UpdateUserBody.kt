package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName

data class UpdateUserBody(

    @SerializedName("userId")
    val userId: Long,


    @SerializedName("name")
    val name: String,
    @SerializedName("fname")
    val fname: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("reagentCode")
    val reagentCode: String


)
