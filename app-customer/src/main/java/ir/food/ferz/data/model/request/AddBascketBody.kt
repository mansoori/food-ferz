package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName

data class AddBascketBody(

    @SerializedName("userId")
    val userId: Long,

    @SerializedName("foodsJson")
    val foodsJson: List<FoodsJson>,

    @SerializedName("addressId")
    val addressId: Long,

    @SerializedName("discountId")
    val discountId: Long

)
