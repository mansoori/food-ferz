package ir.food.ferz.data.utils.database

import ir.food.ferz.data.room.FoodDao
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ir.food.ferz.data.model.response.GetAllFoodResponse
import ir.food.ferz.data.room.FoodStorged

@Database(entities = [FoodStorged::class], version = 1)


abstract class AppDatabase : RoomDatabase() {

    abstract fun foodDao(): FoodDao

//    companion object {
//
////        fun buildDatabase(context: Context): AppDatabase = Room.databaseBuilder(
////            context.applicationContext,
////            AppDatabase::class.java,
////            "foodFerz.db"
////        ).build()
//
//    }

}

