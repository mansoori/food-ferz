package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName

data class FoodsJson(

    @SerializedName("foodId")
    val foodId: Long,

    @SerializedName("count")
    val count: Int,

    @SerializedName("isPizza")
    val isPizza: Boolean


)