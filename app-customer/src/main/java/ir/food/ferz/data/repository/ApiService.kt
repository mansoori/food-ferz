package ir.food.ferz.data.repository

import com.androidnetworking.AndroidNetworking
import ir.food.ferz.shared.utils.API_URL
import com.androidnetworking.error.ANError
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.androidnetworking.utils.ParseUtil
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.Single
import ir.food.ferz.App
import ir.food.ferz.data.model.response.*
import ir.food.ferz.features.base.CookiesInterceptor
import okhttp3.OkHttpClient
import org.json.JSONObject
import retrofit2.http.*
import timber.log.Timber
import com.rx2androidnetworking.Rx2AndroidNetworking
import ir.food.ferz.data.model.request.*
import com.google.gson.JsonParser
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import com.google.gson.JsonElement
import java.lang.Exception
import org.json.JSONArray




class ApiService {

    var httpClient = OkHttpClient.Builder()
        .addInterceptor(
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        ).addInterceptor(CookiesInterceptor())
        .build()

    var apiServicee = ApiClient.getClient(App.context).create(
        ApiServicee::
        class.java
    )

    fun testApi(): Observable<testResponse> =
        Rx2AndroidNetworking.get("http://shop.armood.ir/iSndex.php/api/products")
            .setOkHttpClient(httpClient)

            .build()
            .getObjectObservable(testResponse::class.java)

    fun getBaseInfo(): Observable<BaseInfoResponse> =
        Rx2AndroidNetworking.get(API_URL + "getBaseInfo")
            .setOkHttpClient(httpClient)


            .build()
            .getObjectObservable(BaseInfoResponse::class.java)

    fun getRegister(mobile: String): Observable<RegisterResponse> =
        Rx2AndroidNetworking.get(API_URL + "register")
            .setOkHttpClient(httpClient)


            .addQueryParameter("mobile", mobile)
            .build()
            .getObjectObservable(RegisterResponse::class.java)

    fun getVerify(mobile: String): Observable<VerifyResponse> =
        Rx2AndroidNetworking.get(API_URL + "verify")
            .setOkHttpClient(httpClient)


            .addQueryParameter("mobile", mobile)
            .build()
            .getObjectObservable(VerifyResponse::class.java)

    fun getUpdateUser(userBody: UpdateUserBody): Observable<VerifyResponse> =
        Rx2AndroidNetworking.post(API_URL + "updateUser")
            .setOkHttpClient(httpClient)


            .addUrlEncodeFormBodyParameter(Gson().toJson(userBody))
            .build()
            .getObjectObservable(VerifyResponse::class.java)


    fun getReviews(index: String): Observable<List<ReViewResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getReviews")
            .setOkHttpClient(httpClient)


            .addQueryParameter("index", index)
            .build()
            .getObjectListObservable(ReViewResponse::class.java)

    fun getaddCredits(userId: String, credit: String): Observable<VerifyResponse> =
        Rx2AndroidNetworking.get(API_URL + "addCredits")
            .setOkHttpClient(httpClient)
            .addQueryParameter("userId", userId)
            .addQueryParameter("credit", credit)
            .build()
            .getObjectObservable(VerifyResponse::class.java)

    fun getverfiyDiscount(userId: String, discountCode: String): Observable<VerifyResponse> =
        Rx2AndroidNetworking.get(API_URL + "verfiyDiscount")
            .setOkHttpClient(httpClient)


            .addQueryParameter("userId", userId)
            .addQueryParameter("discountCode", discountCode)
            .build()
            .getObjectObservable(VerifyResponse::class.java)


    fun getProvince(): Observable<List<GetProvinceResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getProvince")
            .setOkHttpClient(httpClient)


            .build()
            .getObjectListObservable(GetProvinceResponse::class.java)

    fun getCity(provinceId: String): Observable<List<GetCityResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getCity")
            .addQueryParameter("provinceId", provinceId)
            .setOkHttpClient(httpClient)
            .build()
            .getObjectListObservable(GetCityResponse::class.java)

    fun getAddress(userId: String): Observable<List<GetAddressResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getAddress")
            .setOkHttpClient(httpClient)
            .addQueryParameter("userId", userId)
            .build()
            .getObjectListObservable(GetAddressResponse::class.java)


    fun getAddBascket(addBascketBody: AddBascketBody): Observable<VerifyResponse> {
//        val customerList = addBascketBody.foodsJson
//        val gson = Gson()
//        val data = gson.toJson(customerList)
//        val jsonArray = JsonParser().parse(data).asJsonArray

        val gson = Gson()

        val listString = gson.toJson(
            addBascketBody.foodsJson,
            object : TypeToken<ArrayList<FoodsJson>>() {

            }.type
        )

        val jsonArray = JSONArray(listString)

//        return      Rx2AndroidNetworking.post(API_URL + "addBasket")
//            .setOkHttpClient(httpClient)
//            .addUrlEncodeFormBodyParameter(addBascketBody)
////              .addJSONArrayBody(jsonArray)
//            .build()
//            .getObjectObservable(VerifyResponse::class.java)


        return apiServicee.getAddBascket(
            addBascketBody.userId,
            jsonArray,
            addBascketBody.addressId,
            addBascketBody.discountId
        )


    }


    fun getAddAdress(addAddressBody: AddAddressBody): Observable<VerifyResponse> =

        apiServicee.getAddAddress(
            addAddressBody.userId, addAddressBody.province, addAddressBody.provinceId, addAddressBody.city,
            addAddressBody.cityId, addAddressBody.address, addAddressBody.isSelect
            , addAddressBody.lat, addAddressBody.lng
        )


    /*     Rx2AndroidNetworking.post(API_URL + "addAddress")
             .setOkHttpClient(httpClient)

             .setContentType("application/x-www-form-urlencoded; charset=UTF-8") // custom ContentType

 //            .addHeaders("Content-Type","application/x-www-form-urlencoded;charset=UTF-8")

 //            .addStringBody(Gson().toJson(addAddressBody))
 //            .addUrlEncodeFormBodyParameter(addAddressBody)
 //            .setContentType("application/json; charset=utf-8") // custom ContentType
             .addStringBody(getStringFromObject(Gson().toJson(addAddressBody)))


             .build()
             .getObjectObservable(
                 VerifyResponse::
                 class.java
             )*/


    fun getStringFromObject(json: String): String {
        val obj = Gson().fromJson(json, JsonObject::class.java)
        val mUrlEncodedFormBodyParameterMap = HashMap<String, String>()
        mUrlEncodedFormBodyParameterMap.putAll(
            ParseUtil
                .getParserFactory()
                .getStringMap(obj)
        )
        var data = ""
        for (item in mUrlEncodedFormBodyParameterMap) {
            if (data.isNotEmpty()) {
                data += "&"
            }
            data += item.key + "=" + item.value
        }
        Timber.d("http Nader Convert =====> $data")
        return data
    }


    fun getUpdateAddress(updateAddressBody: UpdateAddressBody): Observable<VerifyResponse> =
        Rx2AndroidNetworking.post(API_URL + "updateAddresses")
            .setOkHttpClient(httpClient)


            .addUrlEncodeFormBodyParameter(updateAddressBody)
            .build()
            .getObjectObservable(VerifyResponse::class.java)

    fun getDeleteAddress(addressId: String): Observable<VerifyResponse> =
        Rx2AndroidNetworking.get(API_URL + "deleteAddress")
            .setOkHttpClient(httpClient)


            .addQueryParameter("addressId", addressId)
            .build()
            .getObjectObservable(VerifyResponse::class.java)

    fun getAllFoods(
        foodCategoryId: String,
        foodTypeId: String,
        foodId: String,
        isWeeklyFood: String,
        index: String
    ): Observable<List<GetAllFoodResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getAllFoods")

            .setOkHttpClient(httpClient)

            .addQueryParameter("foodCategoryId", foodCategoryId)
            .addQueryParameter("foodTypeId", foodTypeId)
            .addQueryParameter("foodId", foodId)
            .addQueryParameter("isWeeklyFood", isWeeklyFood)
            .addQueryParameter("index", index)
            .build()
            .getObjectListObservable(GetAllFoodResponse::class.java)


    fun getBascket(userId: String): Observable<List<GetBascketResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getBasket")
            .setOkHttpClient(httpClient)


            .addQueryParameter("userId", userId)
            .build()
            .getObjectListObservable(GetBascketResponse::class.java)


    fun getFoodType(foodCategoryId: String): Observable<List<GetFoodTypeResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getFoodsTypes")
            .setOkHttpClient(httpClient)


            .addQueryParameter("foodCategoryId", foodCategoryId)
            .build()
            .getObjectListObservable(GetFoodTypeResponse::class.java)

    fun getUserBuy(userId: String): Observable<List<GetBascketResponse>> =
        Rx2AndroidNetworking.get(API_URL + "getUserBuy")
            .setOkHttpClient(httpClient)
            .addQueryParameter("userId", userId)
            .build()
            .getObjectListObservable(GetBascketResponse::class.java)




    interface ApiServicee {
        // Register new user
        @FormUrlEncoded
        @POST("notes/user/register")
        fun register(@Field("device_id") deviceId: String): Single<User>

        @FormUrlEncoded
        @Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")
        @POST("addAddress")

        fun getAddAddress(
            @Field("userId") userId: Long,
            @Field("province") province: String,
            @Field("provinceId") provinceId: Int,
            @Field("city") city: String,
            @Field("cityId") cityId: Int,
            @Field("address") address: String,
            @Field("isSelect") isSelect: Boolean,
            @Field("lat") lat: String,
            @Field("lng") lng: String
        ): Observable<VerifyResponse>

        @FormUrlEncoded
        @POST("addBasket")

        fun getAddBascket(
            @Field("userId") userId: Long,
            @Field("foodsJson") foodsJson: JSONArray,
            @Field("addressId") addressId: Long,
            @Field("discountId") discountId: Long

        ): Observable<VerifyResponse>


        //    // Create note
        //    @FormUrlEncoded
        //    @POST("notes/new")
        //    Single<Note> createNote(@Field("note") String note);
        //
        //    // Fetch all notes
        //    @GET("notes/all")
        //    Single<List<Note>> fetchAllNotes();
        //
        //    // Update single note
        //    @FormUrlEncoded
        //    @PUT("notes/{id}")
        //    Completable updateNote(@Path("id") int noteId, @Field("note") String note);
        //
        //    // Delete note
        //    @DELETE("notes/{id}")
        //    Completable deleteNote(@Path("id") int noteId);
    }

}

