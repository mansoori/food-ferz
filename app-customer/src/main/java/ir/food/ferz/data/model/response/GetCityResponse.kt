package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class GetCityResponse(


    @SerializedName("id") val id : Long,
    @SerializedName("title") val title : String,
    @SerializedName("province_id") val province_id : Int
)
