package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName

data class SignInBody(

    @SerializedName("mobile")
    val mobile: String,


    @SerializedName("pass")
    val pass: String


)
