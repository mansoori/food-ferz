package ir.food.ferz.data.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mansoori on 10/12/17.
 */
data class testResponse(

    @Expose
    @SerializedName("meta")
    val meta: Meta,
    @Expose
    @SerializedName("result")
    val result: ArrayList<Any>
)