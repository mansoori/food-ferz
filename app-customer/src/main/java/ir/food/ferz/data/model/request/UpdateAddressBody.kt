package ir.food.ferz.data.model.request

import com.google.gson.annotations.SerializedName

data class UpdateAddressBody(

    @SerializedName("addressId")
    val addressId: Long,


    @SerializedName("province")
    val province: String,

    @SerializedName("provinceId")
    val provinceId: Int,


    @SerializedName("city")
    val city: String,

    @SerializedName("cityId")
    val cityId: Int,


    @SerializedName("address")
    val address: String,


    @SerializedName("isSelect")
    val isSelect: Boolean


)
