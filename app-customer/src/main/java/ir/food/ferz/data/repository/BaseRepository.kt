package ir.food.ferz.features.base

import android.content.Context
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import ir.food.ferz.data.prefs.PreferencesHelper
import ir.food.ferz.data.repository.ApiEnums
import ir.food.ferz.data.repository.ApiService
import ir.food.ferz.data.repository.DataField

open class BaseRepository(
    var apiService: ApiService,
    var preferencesHelper: PreferencesHelper,
    var context: Context,
    var dataField: DataField
) {


    //    val errorApisEnum = ArrayList<AppApi>()
    private var compositeDisposable = CompositeDisposable()


    var isLogin
        get() = preferencesHelper.isLogin
        set(value) {
            preferencesHelper.isLogin = value
        }


    fun disposeApi() {
        compositeDisposable.clear()
    }

    fun <T> Observable<T>.runApi(
        it: MutableLiveData<T>,
        isLoadingApi: MutableLiveData<Boolean>,
        errorApi: MutableLiveData<Throwable>,
        apiEnum: ApiEnums

    ) {

        if (apiEnum != ApiEnums.BASE_INFO)

            isLoadingApi.value = true

        compositeDisposable.add(
            subscribeWith(object : DisposableObserver<T>() {
                override fun onComplete() {
                    isLoadingApi.value = false
                }

                override fun onNext(response: T) {
                    it.value = response
                    habdleData(apiEnum)
                }

                override fun onError(e: Throwable) {
                    isLoadingApi.value = false
                    errorApi.value = e
                }
            })
        )
    }


    fun <T> Observable<T>.setWorker(): Observable<T> {
        return this.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun habdleData(apiEnum: ApiEnums) {

        when (apiEnum) {
            ApiEnums.GET_ADD_ADDRESS -> {
                preferencesHelper.isAddressSet = true
            }
            ApiEnums.VERIFY_CODE -> {
                preferencesHelper.isLogin = true
            }
        }
    }
}