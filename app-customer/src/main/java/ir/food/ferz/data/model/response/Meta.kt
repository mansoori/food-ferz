package ir.food.ferz.data.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Meta(

        @Expose
        @SerializedName("msg")
        val message: String,

        @Expose
        @SerializedName("status")
        val metaStatus: String


)
