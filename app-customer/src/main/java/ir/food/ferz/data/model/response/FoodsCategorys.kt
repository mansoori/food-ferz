package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class FoodsCategorys (

	@SerializedName("id") val id : Int,
	@SerializedName("name") val name : String,
	@SerializedName("imageName") val imageName : String
)