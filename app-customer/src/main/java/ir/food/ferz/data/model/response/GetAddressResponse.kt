package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class GetAddressResponse(


    @SerializedName("id") val id: Long,
    @SerializedName("userId") val userId: Long,
    @SerializedName("province") val province: String,
    @SerializedName("city") val city: String,
    @SerializedName("provinceId") val provinceId: Int,
    @SerializedName("cityId") val cityId: Int,
    @SerializedName("address") val address: String,
    @SerializedName("isSelect") val isSelect: Boolean,
    @SerializedName("lat")
    val lat: String,
    @SerializedName("lng")
    val lng: String
)