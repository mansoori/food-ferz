package ir.food.ferz.data.prefs

import ir.food.ferz.shared.utils.PREF_KEY_IS_LOGIN
import android.content.SharedPreferences
import ir.food.ferz.shared.utils.PREF_KEY_IS_ADDRESS_SET
import ir.food.ferz.shared.utils.PREF_KEY_USER_ID
import javax.inject.Inject


class AppPreferencesHelper @Inject constructor(val pref:SharedPreferences) : PreferencesHelper {



//    var pref: SharedPreferences = App.context!!.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)


    override var isLogin: Boolean
        get() = pref.getBoolean(PREF_KEY_IS_LOGIN, false)
        set(value) = pref.edit().putBoolean(PREF_KEY_IS_LOGIN, value).apply()

    override var isAddressSet: Boolean
        get() = pref.getBoolean(PREF_KEY_IS_ADDRESS_SET, false)
        set(value) = pref.edit().putBoolean(PREF_KEY_IS_ADDRESS_SET, value).apply()

    override var userId: Long
        get() = pref.getLong(PREF_KEY_USER_ID,0)
        set(value) = pref.edit().putLong(PREF_KEY_USER_ID, value).apply()


//    override var userProfile: LoginResponse?
//        get() = Gson().fromJson(pref.getString(PREF_KEY_USER_PROFILE, ""), LoginResponse::class.java)
//        set(value) = pref.edit().putString(
//            PREF_KEY_USER_PROFILE,
//            if (value != null) Gson().toJson(value) else ""
//        ).apply()




}