package ir.food.ferz.data.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "foods")

data class FoodStorged(

    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @ColumnInfo(name = "foodCategoryId") val foodCategoryId: Long,
    @ColumnInfo(name = "foodTypeId") val foodTypeId: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "price") val price: Int,
    @ColumnInfo(name = "imageName") val imageName: String,
    @ColumnInfo(name = "isWeeklyFood") val isWeeklyFood: Int,
    @ColumnInfo(name = "isActive") val isActive: Boolean,
    @ColumnInfo(name = "isPizza") val isPizza: Boolean,
    @ColumnInfo(name = "pizzaNum") val pizzaNum: String,
    @ColumnInfo(name = "count") var count: Int
)