package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class GetFoodTypeResponse(

    @SerializedName("id") val id: Int,
    @SerializedName("foodCategoryId") val foodCategoryId: Int,

    @SerializedName("name") val name: String,

    @SerializedName("imageName")
    val imageName: String
)


