package   ir.food.ferz.features.base


import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import java.io.IOException

class CookiesInterceptor() : Interceptor {


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()

        val utf ="application/x-www-form-urlencoded; charset=UTF-8"
                builder.addHeader("Content-Type", utf)
        builder.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")

        Timber.d("http Nader ===============> Added UTF 8")
        return chain.proceed(builder.build())
    }
}