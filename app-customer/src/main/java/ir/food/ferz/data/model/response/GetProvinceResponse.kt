package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class GetProvinceResponse(


    @SerializedName("id") val id : Long,
    @SerializedName("title") val title : String,
    @SerializedName("slug") val slug : String,
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double
)