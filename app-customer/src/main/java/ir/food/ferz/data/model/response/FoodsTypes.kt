package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class FoodsTypes (

	@SerializedName("id") val id : Int,
	@SerializedName("name") val name : String
)