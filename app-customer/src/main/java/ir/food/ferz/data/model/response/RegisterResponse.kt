package ir.food.ferz.data.model.response

import com.google.gson.annotations.SerializedName


data class RegisterResponse (

    @SerializedName("verifyCode") val verifyCode :String,
    @SerializedName("userId") val userId : Long
)