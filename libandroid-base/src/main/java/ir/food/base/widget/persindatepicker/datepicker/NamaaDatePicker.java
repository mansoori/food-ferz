package ir.food.base.widget.persindatepicker.datepicker;


import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import ir.food.base.R;
import ir.food.base.widget.persindatepicker.PersianDatePicker;
import ir.food.base.widget.persindatepicker.util.PersianCalendar;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;


/**
 * Created by root on 8/21/17.
 */

public class NamaaDatePicker extends RelativeLayout implements View.OnClickListener  {


    Context context;

    RelativeLayout rlDate;
    TextView tvTitle;
    TextView tvDate;
    long selectedTimeMiliSecond;

    public NamaaDatePicker(Context context) {
        super(context);
        init(context);
    }

    public NamaaDatePicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
        handleAttrs(context, attrs);
    }

    public NamaaDatePicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        handleAttrs(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NamaaDatePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
        handleAttrs(context, attrs);
    }


    private void init(final Context context) {
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.widget_date_picker, this, true);

        rlDate = (RelativeLayout) findViewById(R.id.rl_date_click);
        tvTitle = (TextView) findViewById(R.id.date_tv_title);
        tvDate = (TextView) findViewById(R.id.date_tv_date);

        rlDate.setOnClickListener(this);
    }

    private void handleAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NamaaDatePicker, 0, 0);
        try {
            String title = a.getString(R.styleable.NamaaDatePicker_date_hint);
            tvTitle.setText(title);
        } catch (Exception ignored) {
        }

        a.recycle();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rl_date_click) {
            showDialogNavTripsLimit();
        }
    }




    private String strSelectedPersianDate = "";
    private void showDialogNavTripsLimit() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_dialog_date_selector);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        final RelativeLayout rlAcceptChangeTripValue = (RelativeLayout) dialog.findViewById(R.id.rl_accept_change_trip_value);
        final ImageView icClose = (ImageView) dialog.findViewById(R.id.ic_close);
        final PersianDatePicker persianDatePicker = (PersianDatePicker) dialog.findViewById(R.id.dp_trip_limit);

//        persianDatePicker.setMinYear(1310);
//        persianDatePicker.setMaxYear(1380);

        if(!strSelectedPersianDate.equals(""))
        {
            int year = Integer.parseInt(strSelectedPersianDate.substring(0,4));
            int month = Integer.parseInt(strSelectedPersianDate.substring(5,7));
            int day = Integer.parseInt(strSelectedPersianDate.substring(8,10));

            PersianCalendar pc = new PersianCalendar();
            pc.setPersianDate(year,month,day);
            persianDatePicker.setDisplayPersianDate(pc);
            /**
             * when you remove leapyear(year kabise) line below unComment and 3 line above comment
             */
//            persianDatePicker.setOnlyDisplayPersianDate(year,month,day);

        }
        else{
            PersianCalendar pc = new PersianCalendar();
            pc.setPersianDate(1340,1,1);
            persianDatePicker.setDisplayPersianDate(pc);

        }

        rlAcceptChangeTripValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                final long selectTimeFrom = persianDatePicker.getDisplayDate().getTime();

                final String strPersianDate = persianDatePicker.getDisplayPersianDate().getPersianShortDate();
                 strSelectedPersianDate = strPersianDate;
/**
 * when want remove leapyear from module uncomment 2 line below and comment 2 line above
 */
//                final String strDisplayOnly = persianDatePicker.getOnlyDisplayPersianDate();
//                strSelectedPersianDate = strDisplayOnly;


                tvDate.setText(strPersianDate);

                selectedTimeMiliSecond = selectTimeFrom / 1000;

                dialog.dismiss();
            }
        });
        icClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    public boolean isFill(){
        if(tvDate.getText().toString().isEmpty()){
            Toast.makeText(getContext(),"لطفا تاریخ تولد را انتخاب کنید.",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public long getBirthTime(){
        return selectedTimeMiliSecond;
    }

    public void setBirthTime(Long time){
        selectedTimeMiliSecond = time;
        long longTime = time *1000L;
        PersianCalendar pc = new PersianCalendar(longTime);
        final String strPersianDate = pc.getPersianShortDate();
        strSelectedPersianDate = strPersianDate;
        tvDate.setText(strPersianDate);

    }


}
