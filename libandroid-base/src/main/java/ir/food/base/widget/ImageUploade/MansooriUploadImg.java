package ir.food.base.widget.ImageUploade;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import ir.food.base.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


public class MansooriUploadImg extends LinearLayout implements View.OnClickListener {
    View llClick;
    int codeImageCroper;
    LinearLayout llContentImageUpload;
    TextView tvTitle;
    String image = null;
    String valueForBase64 = "";

    public MansooriUploadImg(Context context) {
        super(context);
        init(context);
    }

    public MansooriUploadImg(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
        handleAttrs(attrs);
    }

    public MansooriUploadImg(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        handleAttrs(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MansooriUploadImg(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
        handleAttrs(attrs);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.widget_nama_upload_image, this, true);

        tvTitle = view.findViewById(R.id.tv_title);
        llClick = view.findViewById(R.id.widget_rl_click);
        llClick.setOnClickListener(this);

        llContentImageUpload = view.findViewById(R.id.ll_content_image);

    }

    private void handleAttrs(AttributeSet attrs) {

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.NamaaUploadImg);
        int n = ta.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = ta.getIndex(i);
            if (attr == R.styleable.NamaaUploadImg_title_upload) {
                setTitle(ta.getString(attr));
            }
        }

        ta.recycle();

    }

    public void setTitle(String strTitle) {
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(strTitle);
    }

    public void setCodeImageCroper(int code) {
        this.codeImageCroper = code;

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.widget_rl_click) {
            showImageCroper(codeImageCroper, getContext());
        }
    }

    ArrayList<WidgetPhotoRemovable> photoRemovableList = new ArrayList<>();

    public void setImageBitmap(Bitmap bitmap) {

        final WidgetPhotoRemovable photoRemovable = new WidgetPhotoRemovable(getContext());
        photoRemovable.setBitmapImage(bitmap);
        photoRemovable.btnRemove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                llClick.setVisibility(View.VISIBLE);
                llContentImageUpload.removeView(photoRemovable);
                photoRemovableList.remove(photoRemovable);
                image = null;
                clearImageBase64();

            }
        });
        photoRemovableList.add(photoRemovable);
        llContentImageUpload.addView(photoRemovable);
        llClick.setVisibility(View.GONE);
    }
    public void setImageUrl(String imageUrl) {

        final WidgetPhotoRemovable photoRemovable = new WidgetPhotoRemovable(getContext());
        photoRemovable.setImageResource(imageUrl);
        photoRemovable.btnRemove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                llClick.setVisibility(View.VISIBLE);
                llContentImageUpload.removeView(photoRemovable);
                photoRemovableList.remove(photoRemovable);
                image = null;
                clearImageBase64();

            }
        });
        photoRemovableList.add(photoRemovable);
        llContentImageUpload.addView(photoRemovable);
        llClick.setVisibility(View.GONE);
    }
    public ArrayList<WidgetPhotoRemovable> getPhotoRemovableList() {
        return photoRemovableList;
    }

    public String getLisImage() {
        if (getImageBase64() != null) {

            image=getImageBase64();
        }
        return image;
    }

    public void setImageBase64(Bitmap image, Bitmap.CompressFormat compressFormat, Integer quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        String valueForBase64 = "data:image/" + compressFormat.name().toLowerCase() + ";base64," + Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP);
    }


    public String getImageBase64() {
        return valueForBase64;
    }


    public void clearImageBase64() {
        valueForBase64 = null;
    }

    public void showImageCroper(Integer requestCode, Context mContext) {
        Intent intent = CropImage.activity().setShowCropOverlay(true)   // data.getData() >> is Uri of picture picked
                .setScaleType(CropImageView.ScaleType.CENTER_CROP)
                .setAspectRatio(150, 150)
                .getIntent(mContext)
                .setAction(android.provider.MediaStore.INTENT_ACTION_VIDEO_CAMERA);
        ((AppCompatActivity) mContext).startActivityForResult(intent, requestCode);
    }

}
