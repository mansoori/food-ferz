package ir.food.base.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import ir.food.base.R;


/**
 * Created by root on 1/9/18.
 */

public class WidgetOptionDetails extends RelativeLayout {

    TextView tvTitle;
    ImageView ivIcon;


    public WidgetOptionDetails(Context context) {
        super(context);
        init(context);
    }

    public WidgetOptionDetails(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
        handleAttrs(attrs);
    }

    public WidgetOptionDetails(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        handleAttrs(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WidgetOptionDetails(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
        handleAttrs(attrs);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View vParent = inflater.inflate(R.layout.widget_option_icon_sub_title, this, true);

        tvTitle = vParent.findViewById(R.id.tv_title);
        ivIcon = vParent.findViewById(R.id.iv_icon);



    }


    @SuppressLint("CustomViewStyleable")
    private void handleAttrs(AttributeSet attrs) {

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.OptionDetails);
        int n = ta.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = ta.getIndex(i);
            if (attr == R.styleable.OptionDetails_icon_option) {
                int mIconSrc = ta.getResourceId(R.styleable.OptionDetails_icon_option, -1);

                setupIconView(mIconSrc);
            } else if (attr == R.styleable.OptionDetails_title_option) {
                setTitle(ta.getString(attr));
            }
        }

        ta.recycle();

    }

    public void setTitle(String strTitle) {
        tvTitle.setText(strTitle);
    }

    public void setupIconView(int mIconSrc) {
        if (mIconSrc != -1)
            ivIcon.setImageResource(mIconSrc);
    }


}
