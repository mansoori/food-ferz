package ir.food.base.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import ir.food.base.R;

/**
 * Created by root on 1/9/18.
 */

public class WidgetRightPanelMenu extends RelativeLayout {

    TextView tvTitle;
    public ImageView ivIcon;

    public WidgetRightPanelMenu(Context context) {
        super(context);
        init(context);
    }

    public WidgetRightPanelMenu(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
        handleAttrs(attrs);
    }

    public WidgetRightPanelMenu(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        handleAttrs(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WidgetRightPanelMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
        handleAttrs(attrs);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View vParent = inflater.inflate(R.layout.widget_right_panel_menu, this, true);

        tvTitle = vParent.findViewById(R.id.tv_title_menu);
        ivIcon = vParent.findViewById(R.id.iv_menu);

    }


    @SuppressLint("CustomViewStyleable")
    private void handleAttrs(AttributeSet attrs) {

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.RightPanelMenu);
        int n = ta.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = ta.getIndex(i);
            if (attr == R.styleable.RightPanelMenu_src_menu) {
                setupIconView(ta);
            } else if (attr == R.styleable.RightPanelMenu_text_menu) {
                setTitle(ta.getString(attr));
            } else if (attr == R.styleable.RightPanelMenu_padding_menu_ic) {
                setPaddingImage(ta);
            } else if (attr == R.styleable.RightPanelMenu_tint_color) {
                setTintColor(ta);
            }
        }

        ta.recycle();

    }

    public void setTitle(String strTitle) {
        tvTitle.setText(strTitle);
    }


    private void setupIconView(TypedArray attributes) {
        int mIconSrc = attributes.getResourceId(R.styleable.RightPanelMenu_src_menu, -1);
        if (mIconSrc != -1)
            ivIcon.setImageResource(mIconSrc);
    }

    private void setPaddingImage(TypedArray attributes) {

        int pad = attributes.getDimensionPixelSize(R.styleable.RightPanelMenu_padding_menu_ic, 0);
        ivIcon.setPadding(pad, pad, pad, pad);

    }

    private void setTintColor(TypedArray attributes) {

        int tint = attributes.getResourceId(R.styleable.RightPanelMenu_tint_color, 0);
        ivIcon.setColorFilter(getContext().getResources().getColor(tint), android.graphics.PorterDuff.Mode.SRC_IN);


    }

}
