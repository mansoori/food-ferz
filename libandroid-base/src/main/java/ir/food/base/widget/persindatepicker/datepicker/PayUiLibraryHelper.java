package ir.food.base.widget.persindatepicker.datepicker;


import ir.food.base.widget.persindatepicker.util.PersianCalendar;

public class PayUiLibraryHelper {
    PayUiLibraryHelper payUiLibraryHelper;

    public PayUiLibraryHelper getInstance() {

        payUiLibraryHelper = new PayUiLibraryHelper();

        return payUiLibraryHelper;

    }


    public String getBirthTime(Long time) {
        long longTime = time * 1000L;
        PersianCalendar pc = new PersianCalendar(longTime);
        final String strPersianDate = pc.getPersianShortDate();
        return strPersianDate;

    }
}
