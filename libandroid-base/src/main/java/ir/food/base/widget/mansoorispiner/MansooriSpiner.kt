package ir.food.base.widget.mansoorispiner

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import ir.food.base.R
import kotlinx.android.synthetic.main.mansoori_spiner.view.*


class MansooriSpiner : LinearLayout {
    private val ERROR_MSG =
        "Very very very long error message to get scrolling or multiline animation when the error button is clicked"
    private val ITEMS = arrayOf("Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6")

    private var adapter: ArrayAdapter<String>? = null

    private var shown = false

    constructor(context: Context?) : super(context) {
        initView(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView(context)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context)
    }

    fun initView(context: Context?) {
        val inflater = getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val vParent = inflater.inflate(R.layout.mansoori_spiner, this, true)

        adapter =ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, ITEMS)
        adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    }
     fun initSpinnerHintAndCustomHintView() {
        spinner1.setAdapter(adapter)
        spinner1.setHint("Select an item")
    }

     fun initSpinnerHintAndFloatingLabel() {
        spinner1.setAdapter(adapter)
        spinner1.setPaddingSafe(0, 0, 0, 0)
    }

     fun initSpinnerOnlyHint() {

    }

     fun initSpinnerNoHintNoFloatingLabel() {
         spinner1.adapter = adapter
    }

     fun initSpinnerMultiline() {
         spinner1.adapter = adapter
         spinner1.hint = "Select an item"
    }

     fun initSpinnerScrolling() {
         spinner1.adapter = adapter
         spinner1.hint = "Select an item"
    }

     fun initEmptyArray() {
        val emptyArray = arrayOf<String>()
        spinner1.adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, emptyArray)
    }

    fun activateError(view: View) {
        if (!shown) {
            spinner1.error = ERROR_MSG
            spinner1.error = ERROR_MSG
        } else {
            spinner1.error = null
            spinner1.error = null
        }
        shown = !shown
    }
}





