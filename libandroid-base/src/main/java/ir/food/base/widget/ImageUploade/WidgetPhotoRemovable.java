package ir.food.base.widget.ImageUploade;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import ir.food.base.R;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;


public class WidgetPhotoRemovable extends RelativeLayout {

    ImageView btnRemove,imageView;


    public WidgetPhotoRemovable(Context context) {
        super(context);
        init(context);
    }

    public WidgetPhotoRemovable(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public WidgetPhotoRemovable(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WidgetPhotoRemovable(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View vParent = inflater.inflate(R.layout.widget_removable_photo, this, true);
        imageView = vParent.findViewById(R.id.upload_image);
        btnRemove = vParent.findViewById(R.id.btn_remove);

    }


    public ImageView getImageView() {

        return imageView;
    }

    public ImageView getBtnRemove() {
        return btnRemove;
    }

    public void setBitmapImage(Bitmap bitmapImage) {
        imageView.setImageBitmap(bitmapImage);
    }

    public void setImageResource(String urlImage) {
//        Glide.with(getContext())
//                .load(urlImage)
//                .asBitmap()
//                .placeholder(R.drawable.place_holder_pic)
//                .into(imageView);
    }

    public Bitmap getBitmap() {
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        return bitmap;
    }


}
