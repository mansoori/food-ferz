package ir.food.base.widget

import android.annotation.SuppressLint
import android.content.Context
import ir.food.base.callback.MansooriMaterialEdittextOnToch
import androidx.core.content.ContextCompat
import android.text.*
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.rengwuxian.materialedittext.MaterialEditText
import ir.food.base.R
import kotlinx.android.synthetic.main.widget_edit_text.view.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*
import java.util.regex.Pattern


class MansooriMaterialEditText : LinearLayout {
    internal var type = 1
    internal var maxlenght = 0
    internal var isForcible = false
    internal var isAmount = false
    private val prefix: String = ""
    private val MAX_DECIMAL = 3

    private var previousCleanString: String? = null

    var namaaMaterialEdittextOnToch: MansooriMaterialEdittextOnToch? = null

    constructor(context: Context?) : super(context) {
        initView(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView(context)
        handleAttrs(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context)
        handleAttrs(attrs)
    }

    fun initView(context: Context?) {
        val inflater = getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val vParent = inflater.inflate(R.layout.widget_edit_text, this, true)

        edit_text.setOnTouchListener { v, event ->
           error.visibility = View.INVISIBLE
            v?.onTouchEvent(event) ?: true
        }
        edit_text.setOnClickListener {
            namaaMaterialEdittextOnToch?.let {
                it.onToch()

            }
        }
    }

    fun setAmountText(str: String?) {
        amount.text = str
    }

    fun setOnTochClick(namaaMaterialEdittextOnToch: MansooriMaterialEdittextOnToch) {

        this.namaaMaterialEdittextOnToch = namaaMaterialEdittextOnToch
    }

    fun afterTextChanged(afterTextChanged: (String) -> Unit) {
        edit_text.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(editable: Editable?) {
                afterTextChanged.invoke(editable.toString().replace(",".toRegex(), ""))
                setError("")

/////////////////////////////////////////////////////////////////////////
                if (isAmount) {
                    val str = editable.toString()
                    if (str.length < prefix.length) {
                        edit_text.setText(prefix)
                        edit_text.setSelection(prefix.length)
                        return
                    }
                    if (str == prefix) {
                        return
                    }
                    // cleanString this the string which not contain prefix and ,
                    val cleanString = str.replace(prefix, "").replace("[,]".toRegex(), "")
                    // for prevent afterTextChanged recursive call
                    if (cleanString == previousCleanString || cleanString.isEmpty()) {
                        return
                    }
                    previousCleanString = cleanString

                    val formattedString: String
                    if (cleanString.contains(".")) {
                        formattedString = formatDecimal(cleanString)
                    } else {
                        formattedString = formatInteger(cleanString)
                    }
                    edit_text.removeTextChangedListener(this) // Remove listener
                    edit_text.setText(formattedString)
                    handleSelection()
                    edit_text.addTextChangedListener(this) // Add back the listener
                }


            }
        })
    }


    @SuppressLint("CustomViewStyleable")
    private fun handleAttrs(attrs: AttributeSet?) {

        val ta = getContext().obtainStyledAttributes(attrs, R.styleable.NamaaEditText)
        val n = ta.indexCount
        for (i in 0 until n) {
            val attr = ta.getIndex(i)
            when (attr) {

                R.styleable.NamaaEditText_hint_text -> setHint(ta.getString(attr))
                R.styleable.NamaaEditText_edit_text_gravity -> {
                    val gravity = ta.getInt(attr, Gravity.RIGHT)
                    edit_text.gravity = Gravity.CENTER or gravity
                }
                R.styleable.NamaaEditText_edt_type_input -> {
                    setType(ta.getInt(attr, 1))
                }
                R.styleable.NamaaEditText_maxEdtcharacter -> {
                    setEditTextMaxLength(ta.getInteger(attr, 0))
                }
                R.styleable.NamaaEditText_isEdtSingleLine -> {
                    if (ta.getBoolean(attr, true)) {
                        edit_text.setSingleLine()
                    } else {
                        edit_text.setSingleLine(false)
                        setMaxLine(Integer.MAX_VALUE)
                    }
                }
                R.styleable.NamaaEditText_is_amount -> {
                    isAmount = ta.getBoolean(attr, true)
                }
                R.styleable.NamaaEditText_isForcible -> {
                    var isForce = ta.getBoolean(attr, false)
                    setForcible(isForce)
                }
                R.styleable.NamaaEditText_isFloating -> {
                    var isFloat = ta.getBoolean(attr, false)
                    setFloating(isFloat)
                }
                else -> {
                }
            }
        }

        ta.recycle()
    }

    fun isEmpty(): Boolean {
        return edit_text.text.toString().isEmpty()
    }

    fun setMaxLine(valueLine: Int) {
        edit_text.maxLines = valueLine

    }

    fun setEditTextMaxLength(length: Int) {
        maxlenght = length
        val FilterArray = arrayOfNulls<InputFilter>(1)
        FilterArray[0] = InputFilter.LengthFilter(length)
        edit_text.filters = FilterArray
    }

    fun setType(type: Int) {
        this.type = type
        when (type) {
            0 -> edit_text.inputType = InputType.TYPE_CLASS_PHONE
            3 -> edit_text.inputType = InputType.TYPE_CLASS_TEXT
            else -> edit_text.inputType = InputType.TYPE_CLASS_TEXT
        }
    }

    fun setText(str: String?) {
        str?.let {
            edit_text.setText(str)
        } ?: kotlin.run {
            edit_text.setText("")

        }

    }

    fun getText(): String {
        return edit_text.text.toString().trim().replace(",".toRegex(), "")
    }
    fun getTextDecimalFormat(): String {
        return edit_text.text.toString()
    }
    fun setError(errorText: String) {

        if (errorText.isEmpty()) {
            error.visibility = View.INVISIBLE
            edit_text.underlineColor = ContextCompat.getColor(context, R.color.green_200)
        } else {
            edit_text.underlineColor = ContextCompat.getColor(context, R.color.red_900)
            setAmountText(null)
            error.visibility = View.VISIBLE
            error.text = errorText
        }
    }

    fun setHint(str: String) {
        content_edittext.hint = str
        edit_text.floatingLabelText = str

    }

    fun isValid(): Boolean {
        var isValid = true
        if (isForcible && TextUtils.isEmpty(edit_text.text.toString())) {
            setError("empty_value_input")
            isValid = false
        }
        return isValid
    }

    fun setForcible(b: Boolean) {
        this.isForcible = b
    }

    @SuppressLint("WrongConstant")
    fun setFloating(b: Boolean) {
        if (b)
            edit_text.setFloatingLabel(1)
        else
            content_edittext.isHintEnabled = false

        edit_text.setFloatingLabel(0)

    }

    fun MaterialEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(editable: Editable?) {

                afterTextChanged.invoke(editable.toString())
                error = null


            }
        })
    }

    fun isEmailValid(email: String): Boolean {

        val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")

        val pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)

        if (TextUtils.isEmpty(email))
            return true
        else if (matcher.matches())
            return true
        else
            setError("invalidData")
        return false
    }

    fun isValidPayCashOut(minCreditWithdraw:String): Boolean {

        return when {
            edit_text.text.toString().isEmpty() -> {
                setError("empty_value_input")
                false
            }
            edit_text.text.toString().startsWith("0") -> {
                showMessage("pay_can_not_zero")
                false
            }
            edit_text.text!!.replace(",".toRegex(), "").toInt()< minCreditWithdraw.toInt()-> {
                setError("حداقل مبلغ وارد شده باید $minCreditWithdraw تومان باشد.")
                false
            }
            else -> true
        }

    }

    fun isValiNationalCard(): Boolean {
        return if (TextUtils.isEmpty(edit_text.text.toString())) {
            setError("empty_value_input")
            false
        } else if (!TextUtils.isEmpty(edit_text.text.toString()) && (edit_text.length() == 10)) {
            true
        } else {
            setError("invalidData")
            false
        }
    }

    fun doubleToStringNoDecimal(d: Double): String {
        val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
        formatter.applyPattern("#,###")
        return formatter.format(d)
    }

    fun isValidPayValue(minPayValid:String,maxPayValid:String): Boolean {
        return if (!TextUtils.isEmpty(edit_text.text.toString())) {

            if (edit_text.text!!.startsWith("0")) {

                showMessage("pay_can_not_zero")
                false
            }
            else if (edit_text.text!!.replace(",".toRegex(), "").toInt()< minPayValid.toInt()) {
                setError("حداقل مبلغ  $minPayValid تومان میباشد.")
                false
            }
//            else if (edit_text.text!!.replace(",".toRegex(), "").toInt() > maxPayValid.toInt()) {
//                setError("حداکثر مبلغ  $maxPayValid تومان میباشد.")
//                false
//            }
           else {
                true
            }

        } else {
            setError("empty_value_input")
            false
        }
    }

    private fun showMessage(str: String) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show()
    }


    private fun handleCaseCurrencyEmpty(focused: Boolean) {
        if (focused) {
            if (getText().isEmpty()) {
                setText(prefix)
            }
        } else {
            if (getText() == prefix) {
                setText("")
            }
        }
    }


    private fun formatInteger(str: String): String {
        val parsed = BigDecimal(str)
        val formatter = DecimalFormat("$prefix#,###", DecimalFormatSymbols(Locale.US))
        return formatter.format(parsed)
    }

    private fun formatDecimal(str: String): String {
        if (str == ".") {
            return "$prefix."
        }
        val parsed = BigDecimal(str)
        // example pattern VND #,###.00
        val formatter = DecimalFormat(
            prefix + "#,###." + getDecimalPattern(str),
            DecimalFormatSymbols(Locale.US)
        )
        formatter.roundingMode = RoundingMode.DOWN
        return formatter.format(parsed)
    }

    /**
     * It will return suitable pattern for format decimal
     * For example: 10.2 -> return 0 | 10.23 -> return 00, | 10.235 -> return 000
     */
    private fun getDecimalPattern(str: String): String {
        val decimalCount = str.length - str.indexOf(".") - 1
        val decimalPattern = StringBuilder()
        var i = 0
        while (i < decimalCount && i < MAX_DECIMAL) {
            decimalPattern.append("0")
            i++
        }
        return decimalPattern.toString()
    }

    private fun handleSelection() {
        if (edit_text.getText()!!.length <= 11) {
            edit_text.setSelection(edit_text.getText()!!.length)
        } else {
            edit_text.setSelection(11)
        }
    }

}