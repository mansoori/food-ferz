package ir.food.base.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;

public class CustomeTextView extends AppCompatTextView {

        public CustomeTextView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public CustomeTextView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public CustomeTextView(Context context) {
            super(context);
            init();
        }

        public void init() {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Vazir-Bold.ttf");
            setTypeface(tf );

        }
    }
